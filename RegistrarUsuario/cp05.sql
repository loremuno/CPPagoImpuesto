/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.13-MariaDB : Database - pagoimpuesto
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pagoimpuesto` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pagoimpuesto`;

/*Table structure for table `atributo` */

DROP TABLE IF EXISTS `atributo`;

CREATE TABLE `atributo` (
  `OIDAtributo` varchar(32) NOT NULL,
  `codigoAtributo` int(11) NOT NULL,
  `fechaHabilitacionAtributo` date NOT NULL,
  `fechaInhabilitacionAtributo` date DEFAULT NULL,
  `longitudAtributo` int(11) DEFAULT NULL,
  `nombreAtributo` varchar(45) NOT NULL,
  `OIDTipoDato` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`OIDAtributo`),
  UNIQUE KEY `UK_d0yib8j696bhxicp6187olcbn` (`codigoAtributo`),
  KEY `FK_bbgko1mw0ucuoga8wiovkxj75` (`OIDTipoDato`),
  CONSTRAINT `FK_bbgko1mw0ucuoga8wiovkxj75` FOREIGN KEY (`OIDTipoDato`) REFERENCES `tipodato` (`OIDTipoDato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `atributo` */

insert  into `atributo`(`OIDAtributo`,`codigoAtributo`,`fechaHabilitacionAtributo`,`fechaInhabilitacionAtributo`,`longitudAtributo`,`nombreAtributo`,`OIDTipoDato`) values ('1',1,'2017-10-02',NULL,NULL,'Importe','2'),('2',2,'2017-10-02',NULL,NULL,'Fecha de vencimiento','1'),('3',3,'2017-10-02',NULL,NULL,'Fecha proximo vencimiento','1'),('4',4,'2017-10-02',NULL,NULL,'Monto minimo exigible','2');

/*Table structure for table `banco` */

DROP TABLE IF EXISTS `banco`;

CREATE TABLE `banco` (
  `OIDBanco` varchar(32) NOT NULL,
  `fechaHabilitacionBanco` date DEFAULT NULL,
  `fechaInhabilitacionBanco` date DEFAULT NULL,
  `cuitBanco` varchar(255) DEFAULT NULL,
  `nombreBanco` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDBanco`),
  UNIQUE KEY `UK_1c70fmn989x0w3x3oe6kh3xex` (`cuitBanco`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `banco` */

insert  into `banco`(`OIDBanco`,`fechaHabilitacionBanco`,`fechaInhabilitacionBanco`,`cuitBanco`,`nombreBanco`) values ('1','2017-10-02',NULL,'1','Banco Nacion');

/*Table structure for table `cliente` */

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `OIDCliente` varchar(32) NOT NULL,
  `apellidoCliente` varchar(255) NOT NULL,
  `dniCliente` int(11) NOT NULL,
  `nombreCliente` varchar(255) NOT NULL,
  `numeroCliente` varchar(255) NOT NULL,
  `fechaHabilitacionCliente` date DEFAULT NULL,
  `fechaInhabilitacionCliente` date DEFAULT NULL,
  PRIMARY KEY (`OIDCliente`),
  UNIQUE KEY `UK_spjnf3pklvp1902gxxbhksaob` (`numeroCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cliente` */

insert  into `cliente`(`OIDCliente`,`apellidoCliente`,`dniCliente`,`nombreCliente`,`numeroCliente`,`fechaHabilitacionCliente`,`fechaInhabilitacionCliente`) values ('1','Muñoz',36731121,'Lorenzo','1','2017-10-02',NULL);

/*Table structure for table `comision` */

DROP TABLE IF EXISTS `comision`;

CREATE TABLE `comision` (
  `OIDComision` varchar(255) NOT NULL,
  `valorcomision` double NOT NULL,
  `porcentajeUtilizado` double NOT NULL,
  `OIDOperacion` varchar(255) DEFAULT NULL,
  `OIDComisionEmpresaSistema` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDComision`),
  KEY `FK_7o63e1rj32iyd5ohqmj8lfx61` (`OIDOperacion`),
  KEY `FK_myhupess14vq3vgqhw7xcu7an` (`OIDComisionEmpresaSistema`),
  CONSTRAINT `FK_7o63e1rj32iyd5ohqmj8lfx61` FOREIGN KEY (`OIDOperacion`) REFERENCES `operacion` (`OIDOperacion`),
  CONSTRAINT `FK_myhupess14vq3vgqhw7xcu7an` FOREIGN KEY (`OIDComisionEmpresaSistema`) REFERENCES `comisionempresasistema` (`OIDComisionEmpresaSistema`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `comision` */

insert  into `comision`(`OIDComision`,`valorcomision`,`porcentajeUtilizado`,`OIDOperacion`,`OIDComisionEmpresaSistema`) values ('402881e95f0401c6015f0401c8a00002',2.5,0.5,'402881e95f035428015f035459160000','402881e95f0401c6015f0401c89c0000');

/*Table structure for table `comisionempresasistema` */

DROP TABLE IF EXISTS `comisionempresasistema`;

CREATE TABLE `comisionempresasistema` (
  `OIDComisionEmpresaSistema` varchar(255) NOT NULL,
  `codigoces` int(11) NOT NULL,
  `fechacomisionempresasistema` date NOT NULL,
  `OIDEmpresaTipoImpuesto` varchar(255) DEFAULT NULL,
  `periodicidadUtilizada` int(11) NOT NULL,
  PRIMARY KEY (`OIDComisionEmpresaSistema`),
  KEY `FK_8kqpv80oul55hms4gjvsi302` (`OIDEmpresaTipoImpuesto`),
  CONSTRAINT `FK_8kqpv80oul55hms4gjvsi302` FOREIGN KEY (`OIDEmpresaTipoImpuesto`) REFERENCES `empresatipoimpuesto` (`OIDEmpresaTipoImpuesto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `comisionempresasistema` */

insert  into `comisionempresasistema`(`OIDComisionEmpresaSistema`,`codigoces`,`fechacomisionempresasistema`,`OIDEmpresaTipoImpuesto`,`periodicidadUtilizada`) values ('402881e95f0401c6015f0401c89c0000',1,'2017-10-09','2',2);

/*Table structure for table `comisionempresasistemaestado` */

DROP TABLE IF EXISTS `comisionempresasistemaestado`;

CREATE TABLE `comisionempresasistemaestado` (
  `OIDComisionEmpresaSistemaEstado` varchar(255) NOT NULL,
  `codigoces` int(11) NOT NULL,
  `nombreEstadoCES` varchar(255) NOT NULL,
  PRIMARY KEY (`OIDComisionEmpresaSistemaEstado`),
  UNIQUE KEY `UK_lijot92p9rcrgrdlgibbf58u7` (`codigoces`),
  UNIQUE KEY `UK_3a63e5ekeqx7l0p0khd3w1cw4` (`nombreEstadoCES`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `comisionempresasistemaestado` */

insert  into `comisionempresasistemaestado`(`OIDComisionEmpresaSistemaEstado`,`codigoces`,`nombreEstadoCES`) values ('1',1,'Pendiente'),('2',2,'Anulada'),('3',3,'Confirmada'),('4',4,'Recalculado');

/*Table structure for table `cuenta` */

DROP TABLE IF EXISTS `cuenta`;

CREATE TABLE `cuenta` (
  `OIDCuenta` varchar(32) NOT NULL,
  `fechaHabilitacionCuenta` date DEFAULT NULL,
  `fechaInhabilitacionCuenta` date DEFAULT NULL,
  `numeroCuenta` int(11) DEFAULT NULL,
  `OIDTipoCuenta` varchar(32) DEFAULT NULL,
  `OIDCliente` varchar(32) DEFAULT NULL,
  `OIDBanco` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`OIDCuenta`),
  KEY `FK_nsw6u6h0f2y7yrcxq99venu5y` (`OIDTipoCuenta`),
  KEY `FK_3qxiemyc9uaws4aw8mbe9iy9c` (`OIDCliente`),
  KEY `FK_psr41x6qd7t1m3i7jy4p6qx2s` (`OIDBanco`),
  CONSTRAINT `FK_3qxiemyc9uaws4aw8mbe9iy9c` FOREIGN KEY (`OIDCliente`) REFERENCES `cliente` (`OIDCliente`),
  CONSTRAINT `FK_nsw6u6h0f2y7yrcxq99venu5y` FOREIGN KEY (`OIDTipoCuenta`) REFERENCES `tipocuenta` (`OIDTipoCuenta`),
  CONSTRAINT `FK_psr41x6qd7t1m3i7jy4p6qx2s` FOREIGN KEY (`OIDBanco`) REFERENCES `banco` (`OIDBanco`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cuenta` */

insert  into `cuenta`(`OIDCuenta`,`fechaHabilitacionCuenta`,`fechaInhabilitacionCuenta`,`numeroCuenta`,`OIDTipoCuenta`,`OIDCliente`,`OIDBanco`) values ('1','2017-10-02',NULL,1,'1','1','1');

/*Table structure for table `empresa` */

DROP TABLE IF EXISTS `empresa`;

CREATE TABLE `empresa` (
  `OIDEmpresa` varchar(255) NOT NULL,
  `cuitEmpresa` varchar(255) NOT NULL,
  `fechaHabilitacionEmpresa` date DEFAULT NULL,
  `fechaInhabilitacionEmpresa` date DEFAULT NULL,
  `nombreEmpresa` varchar(50) NOT NULL,
  `OIDTipoEmpresa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDEmpresa`),
  UNIQUE KEY `UK_eaihovwjj0b6ipcwuau45fm98` (`cuitEmpresa`),
  KEY `FK_j2x2dwvhwnlxrakeev5f41x69` (`OIDTipoEmpresa`),
  CONSTRAINT `FK_j2x2dwvhwnlxrakeev5f41x69` FOREIGN KEY (`OIDTipoEmpresa`) REFERENCES `tipoempresa` (`OIDTipoEmpresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `empresa` */

insert  into `empresa`(`OIDEmpresa`,`cuitEmpresa`,`fechaHabilitacionEmpresa`,`fechaInhabilitacionEmpresa`,`nombreEmpresa`,`OIDTipoEmpresa`) values ('1','1','2017-10-02',NULL,'Claro','1'),('2','2','2017-10-02',NULL,'ECOGas','1'),('3','3','2017-10-02',NULL,'Movistar','1'),('402881e95f306bae015f306c25c20000','112233','2017-10-18',NULL,'MaxiSA','1');

/*Table structure for table `empresatipoimpuesto` */

DROP TABLE IF EXISTS `empresatipoimpuesto`;

CREATE TABLE `empresatipoimpuesto` (
  `OIDEmpresaTipoImpuesto` varchar(255) NOT NULL,
  `codigoempresatipoimpuesto` int(11) NOT NULL,
  `periodoLiquidacion` int(11) DEFAULT NULL,
  `fechaInhabilitacionEmpresaTipoImpuesto` date DEFAULT NULL,
  `fechaHabilitacionEmpresaTipoImpuesto` date NOT NULL,
  `OIDTipoImpuesto` varchar(255) DEFAULT NULL,
  `OIDEmpresa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDEmpresaTipoImpuesto`),
  UNIQUE KEY `UK_qr2t4on5qvvo0kqky7hj14ohd` (`codigoempresatipoimpuesto`),
  KEY `FK_3vsmpg3qu09pxcr59r7jwq7wt` (`OIDTipoImpuesto`),
  KEY `FK_9rfnwxqnfdt39xws6vuurhq37` (`OIDEmpresa`),
  CONSTRAINT `FK_3vsmpg3qu09pxcr59r7jwq7wt` FOREIGN KEY (`OIDTipoImpuesto`) REFERENCES `tipoimpuesto` (`OIDTipoImpuesto`),
  CONSTRAINT `FK_9rfnwxqnfdt39xws6vuurhq37` FOREIGN KEY (`OIDEmpresa`) REFERENCES `empresa` (`OIDEmpresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `empresatipoimpuesto` */

insert  into `empresatipoimpuesto`(`OIDEmpresaTipoImpuesto`,`codigoempresatipoimpuesto`,`periodoLiquidacion`,`fechaInhabilitacionEmpresaTipoImpuesto`,`fechaHabilitacionEmpresaTipoImpuesto`,`OIDTipoImpuesto`,`OIDEmpresa`) values ('1',1,1,NULL,'2017-10-02','1','1'),('2',2,2,NULL,'2017-08-01','2','2'),('3',3,1,NULL,'2017-10-02','1','3'),('402881e95f30af78015f30afce300000',4,1,'2017-10-18','2017-10-18','1','402881e95f306bae015f306c25c20000');

/*Table structure for table `empresatipoimpuestoatributo` */

DROP TABLE IF EXISTS `empresatipoimpuestoatributo`;

CREATE TABLE `empresatipoimpuestoatributo` (
  `OIDEmpresaTipoImpuestoAtributo` varchar(255) NOT NULL,
  `orden` int(11) DEFAULT NULL,
  `formato` varchar(255) DEFAULT NULL,
  `valorPeriodicidad` varchar(255) DEFAULT NULL,
  `OIDTipoImpuestoAtributo` varchar(32) DEFAULT NULL,
  `OIDEmpresaTipoImpuesto` varchar(255) NOT NULL,
  PRIMARY KEY (`OIDEmpresaTipoImpuestoAtributo`),
  KEY `FK_2q0tsyx31dogcnrgrff9opqua` (`OIDTipoImpuestoAtributo`),
  KEY `FK_m3qv64ld6mksnetr1ob1dtm2b` (`OIDEmpresaTipoImpuesto`),
  CONSTRAINT `FK_2q0tsyx31dogcnrgrff9opqua` FOREIGN KEY (`OIDTipoImpuestoAtributo`) REFERENCES `tipoimpuestoatributo` (`OIDTipoImpuestoAtributo`),
  CONSTRAINT `FK_m3qv64ld6mksnetr1ob1dtm2b` FOREIGN KEY (`OIDEmpresaTipoImpuesto`) REFERENCES `empresatipoimpuesto` (`OIDEmpresaTipoImpuesto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `empresatipoimpuestoatributo` */

insert  into `empresatipoimpuestoatributo`(`OIDEmpresaTipoImpuestoAtributo`,`orden`,`formato`,`valorPeriodicidad`,`OIDTipoImpuestoAtributo`,`OIDEmpresaTipoImpuesto`) values ('1',0,NULL,NULL,'1','1'),('10',3,NULL,NULL,'4','3'),('2',1,NULL,NULL,'2','1'),('3',2,NULL,NULL,'3','1'),('4',3,NULL,NULL,'4','1'),('402881e95f313d39015f313dbcfc0000',0,NULL,NULL,'1','402881e95f30af78015f30afce300000'),('402881e95f313d39015f313dbd020001',1,NULL,NULL,'2','402881e95f30af78015f30afce300000'),('402881e95f313d39015f313dbd050002',2,NULL,NULL,'3','402881e95f30af78015f30afce300000'),('5',0,NULL,NULL,'5','2'),('6',1,NULL,NULL,'6','2'),('7',0,NULL,NULL,'1','3'),('8',1,NULL,NULL,'2','3'),('9',2,NULL,NULL,'3','3');

/*Table structure for table `estadocomisionempresasistema` */

DROP TABLE IF EXISTS `estadocomisionempresasistema`;

CREATE TABLE `estadocomisionempresasistema` (
  `OIDEstadoComisionEmpresaSistema` varchar(255) NOT NULL,
  `fechaestadocesdesde` date DEFAULT NULL,
  `fechaestadoceshasta` date DEFAULT NULL,
  `OIDComisionEmpresaSistemaEstado` varchar(255) DEFAULT NULL,
  `OIDComisionEmpresaSistema` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDEstadoComisionEmpresaSistema`),
  KEY `FK_tid1eo550bguaqu6ey2q7ws4y` (`OIDComisionEmpresaSistemaEstado`),
  KEY `FK_tco1v39gugx2uhm8lm0c6shhd` (`OIDComisionEmpresaSistema`),
  CONSTRAINT `FK_tco1v39gugx2uhm8lm0c6shhd` FOREIGN KEY (`OIDComisionEmpresaSistema`) REFERENCES `comisionempresasistema` (`OIDComisionEmpresaSistema`),
  CONSTRAINT `FK_tid1eo550bguaqu6ey2q7ws4y` FOREIGN KEY (`OIDComisionEmpresaSistemaEstado`) REFERENCES `comisionempresasistemaestado` (`OIDComisionEmpresaSistemaEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `estadocomisionempresasistema` */

insert  into `estadocomisionempresasistema`(`OIDEstadoComisionEmpresaSistema`,`fechaestadocesdesde`,`fechaestadoceshasta`,`OIDComisionEmpresaSistemaEstado`,`OIDComisionEmpresaSistema`) values ('402881e95f0401c6015f0401c8a00001','2017-10-09','2017-10-09','1','402881e95f0401c6015f0401c89c0000'),('402881e95f0401fb015f04026ad60000','2017-10-09',NULL,'3','402881e95f0401c6015f0401c89c0000');

/*Table structure for table `operacion` */

DROP TABLE IF EXISTS `operacion`;

CREATE TABLE `operacion` (
  `OIDOperacion` varchar(255) NOT NULL,
  `codpagoelect` varchar(255) NOT NULL,
  `fechavencimientocomprobanteimpago` date NOT NULL,
  `fechaoperacion` date NOT NULL,
  `importeOperacion` double NOT NULL,
  `numeroComprobante` int(11) NOT NULL,
  `ComisionCalculadaOperacion` bit(1) DEFAULT NULL,
  `numerooperacion` int(11) NOT NULL,
  `OIDTipoImpuesto` varchar(255) DEFAULT NULL,
  `OIDEmpresaTipoImpuesto` varchar(255) DEFAULT NULL,
  `OIDCuenta` varchar(32) DEFAULT NULL,
  `OIDCliente` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`OIDOperacion`),
  KEY `FK_snguttj6rd8i3e75n1dl3bvx8` (`OIDTipoImpuesto`),
  KEY `FK_53os52ht01w28nbseqfk4qtpe` (`OIDEmpresaTipoImpuesto`),
  KEY `FK_njd548o9jlyb9ebfk9nsug2o2` (`OIDCuenta`),
  KEY `FK_9qxaeeke83rxu6yh5t454508f` (`OIDCliente`),
  CONSTRAINT `FK_53os52ht01w28nbseqfk4qtpe` FOREIGN KEY (`OIDEmpresaTipoImpuesto`) REFERENCES `empresatipoimpuesto` (`OIDEmpresaTipoImpuesto`),
  CONSTRAINT `FK_9qxaeeke83rxu6yh5t454508f` FOREIGN KEY (`OIDCliente`) REFERENCES `cliente` (`OIDCliente`),
  CONSTRAINT `FK_njd548o9jlyb9ebfk9nsug2o2` FOREIGN KEY (`OIDCuenta`) REFERENCES `cuenta` (`OIDCuenta`),
  CONSTRAINT `FK_snguttj6rd8i3e75n1dl3bvx8` FOREIGN KEY (`OIDTipoImpuesto`) REFERENCES `tipoimpuesto` (`OIDTipoImpuesto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `operacion` */

insert  into `operacion`(`OIDOperacion`,`codpagoelect`,`fechavencimientocomprobanteimpago`,`fechaoperacion`,`importeOperacion`,`numeroComprobante`,`ComisionCalculadaOperacion`,`numerooperacion`,`OIDTipoImpuesto`,`OIDEmpresaTipoImpuesto`,`OIDCuenta`,`OIDCliente`) values ('402880eb5f416e93015f416f87d30000','123','2017-12-12','2017-10-21',75,1,'\0',9,'1','1','1','1'),('402880eb5f4bdc21015f4bdd3ad60000','123','2017-12-12','2017-10-23',16,1,'\0',9,'1','1','1','1'),('402880eb5f4bdc21015f4bdda93e0003','123','2017-12-13','2017-10-23',100,4,'\0',9,'2','2','1','1'),('402881e95f035428015f035459160000','123','2017-10-09','2017-08-01',500,1,'',1,'2','2','1','1'),('402881e95f03fabf015f03fb07f00000','123','2017-10-09','2017-10-09',500,1,'\0',2,'2','2','1','1'),('402881e95f32681f015f326866770000','123','2018-06-06','2017-10-18',100,1,'\0',3,'1','1','1','1'),('402881e95f32681f015f32689fa70002','123','0018-06-09','2017-10-18',100,1,'\0',4,'2','2','1','1'),('402881e95f32681f015f3268f2020003','123','0018-06-09','2017-10-18',300,3,'\0',5,'1','3','1','1'),('402881e95f327768015f327b459a0000','123','0018-06-09','2017-10-18',50,1,'\0',6,'1','3','1','1'),('402881e95f32a72e015f32a780ab0000','123','0018-06-09','2017-10-19',45,3,'\0',7,'1','1','1','1'),('402881e95f32bbec015f32bcb1920000','123','2017-12-12','2017-10-19',200,2,'\0',8,'1','1','1','1');

/*Table structure for table `operacionatributo` */

DROP TABLE IF EXISTS `operacionatributo`;

CREATE TABLE `operacionatributo` (
  `OIDOperacionAtributo` varchar(32) NOT NULL,
  `valorOperacionAtibuto` varchar(255) NOT NULL,
  `OIDTipoImpuestoAtributo` varchar(32) DEFAULT NULL,
  `OIDOperacion` varchar(255) NOT NULL,
  PRIMARY KEY (`OIDOperacionAtributo`),
  KEY `FK_qkhrhh46a9h00sain16mthy3u` (`OIDTipoImpuestoAtributo`),
  KEY `FK_of60sp4x3udcwwilb6kgbfyic` (`OIDOperacion`),
  CONSTRAINT `FK_of60sp4x3udcwwilb6kgbfyic` FOREIGN KEY (`OIDOperacion`) REFERENCES `operacion` (`OIDOperacion`),
  CONSTRAINT `FK_qkhrhh46a9h00sain16mthy3u` FOREIGN KEY (`OIDTipoImpuestoAtributo`) REFERENCES `tipoimpuestoatributo` (`OIDTipoImpuestoAtributo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `operacionatributo` */

insert  into `operacionatributo`(`OIDOperacionAtributo`,`valorOperacionAtibuto`,`OIDTipoImpuestoAtributo`,`OIDOperacion`) values ('402880eb5f416e93015f416f87d80001','2018-01-01T00:00:00-03:00','3','402880eb5f416e93015f416f87d30000'),('402880eb5f416e93015f416f87d90002','50.0','4','402880eb5f416e93015f416f87d30000'),('402880eb5f4bdc21015f4bdd3ad60001','2018-01-01T00:00:00-03:00','3','402880eb5f4bdc21015f4bdd3ad60000'),('402880eb5f4bdc21015f4bdd3ad60002','12.5','4','402880eb5f4bdc21015f4bdd3ad60000'),('402881e95f32681f015f3268667f0001','50.0','3','402881e95f32681f015f326866770000'),('402881e95f32681f015f3268f2020004','120.0','4','402881e95f32681f015f3268f2020003'),('402881e95f327768015f327b459f0001','50.0','4','402881e95f327768015f327b459a0000'),('402881e95f32a72e015f32a780b30001','2018-01-01T00:00:00-03:00','3','402881e95f32a72e015f32a780ab0000'),('402881e95f32a72e015f32a780b30002','45.0','4','402881e95f32a72e015f32a780ab0000'),('402881e95f32bbec015f32bcb1a20001','2018-01-01T00:00:00-03:00','3','402881e95f32bbec015f32bcb1920000'),('402881e95f32bbec015f32bcb1a20002','20.0','4','402881e95f32bbec015f32bcb1920000');

/*Table structure for table `parametroconexion` */

DROP TABLE IF EXISTS `parametroconexion`;

CREATE TABLE `parametroconexion` (
  `OIDParametroConexion` varchar(255) NOT NULL,
  `codigoConexion` varchar(255) NOT NULL,
  `codigoParametroConexion` int(11) NOT NULL,
  `fechaInhabilitacionParametroConexion` date DEFAULT NULL,
  `fechaHabilitacionParametroConexion` date NOT NULL,
  PRIMARY KEY (`OIDParametroConexion`),
  UNIQUE KEY `UK_fscgv0hrq02v1vrgciva20sno` (`codigoConexion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `parametroconexion` */

insert  into `parametroconexion`(`OIDParametroConexion`,`codigoConexion`,`codigoParametroConexion`,`fechaInhabilitacionParametroConexion`,`fechaHabilitacionParametroConexion`) values ('1','1',1,NULL,'2017-10-02');

/*Table structure for table `parametroporeditable` */

DROP TABLE IF EXISTS `parametroporeditable`;

CREATE TABLE `parametroporeditable` (
  `OIDParametroPorEditable` varchar(255) NOT NULL,
  `porcentajecon` double NOT NULL,
  `porcentajesin` double NOT NULL,
  PRIMARY KEY (`OIDParametroPorEditable`),
  UNIQUE KEY `UK_eyyr02vvfglsxbsnpv7tmate6` (`porcentajecon`),
  UNIQUE KEY `UK_d4e7etita7nbldiogedy7f4um` (`porcentajesin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `parametroporeditable` */

insert  into `parametroporeditable`(`OIDParametroPorEditable`,`porcentajecon`,`porcentajesin`) values ('402881e95f352ad2015f352aff0e0000',1.2,3.5);

/*Table structure for table `parametroporperiodicidad` */

DROP TABLE IF EXISTS `parametroporperiodicidad`;

CREATE TABLE `parametroporperiodicidad` (
  `OIDParametroPorPeriodicidad` varchar(255) NOT NULL,
  `porcentajeanual` double NOT NULL,
  `porcentajemensual` double NOT NULL,
  `porcentajebimestral` double NOT NULL,
  `porcentajetrimestral` double NOT NULL,
  PRIMARY KEY (`OIDParametroPorPeriodicidad`),
  UNIQUE KEY `UK_h2kbxlkifrjhtws5hxllv9x1n` (`porcentajeanual`),
  UNIQUE KEY `UK_9civj7aexs15hyc7l0u5u40r3` (`porcentajemensual`),
  UNIQUE KEY `UK_l4txao9cmcnj04ovgi6fm929j` (`porcentajebimestral`),
  UNIQUE KEY `UK_tabuncjoawx772i2uy5wvd86o` (`porcentajetrimestral`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `parametroporperiodicidad` */

insert  into `parametroporperiodicidad`(`OIDParametroPorPeriodicidad`,`porcentajeanual`,`porcentajemensual`,`porcentajebimestral`,`porcentajetrimestral`) values ('1',0.5,0.5,0.5,0.5),('402881e95f356fad015f356fd9ae0000',2.1,6.6,1.1,3.4),('402881e95f35705b015f357099650000',5.8,10.8,7.9,2);

/*Table structure for table `rol` */

DROP TABLE IF EXISTS `rol`;

CREATE TABLE `rol` (
  `OIDRol` varchar(32) NOT NULL,
  `fechaHabilitacionRol` date NOT NULL,
  `fechaInhabilitacionRol` date DEFAULT NULL,
  `codigoRol` int(11) DEFAULT NULL,
  `nombreRol` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDRol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rol` */

insert  into `rol`(`OIDRol`,`fechaHabilitacionRol`,`fechaInhabilitacionRol`,`codigoRol`,`nombreRol`) values ('1','2017-10-02',NULL,1,'ClienteBanco');

/*Table structure for table `rolopcion` */

DROP TABLE IF EXISTS `rolopcion`;

CREATE TABLE `rolopcion` (
  `OIDRolOpcion` varchar(32) NOT NULL,
  `fechaHabilitacionRolOpcion` date NOT NULL,
  `fechaInhabilitacionRolOpcion` date DEFAULT NULL,
  `codigoRolOpcion` int(11) DEFAULT NULL,
  `nombreRolOpcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDRolOpcion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rolopcion` */

insert  into `rolopcion`(`OIDRolOpcion`,`fechaHabilitacionRolOpcion`,`fechaInhabilitacionRolOpcion`,`codigoRolOpcion`,`nombreRolOpcion`) values ('2','2017-10-02',NULL,2,'CUPAGARIMPUESTO');

/*Table structure for table `rolrolopcion` */

DROP TABLE IF EXISTS `rolrolopcion`;

CREATE TABLE `rolrolopcion` (
  `OIDRol` varchar(32) NOT NULL,
  `OIDRolOpcion` varchar(32) NOT NULL,
  KEY `FK_dhrswql6a0tbxcvgx7ffayu2d` (`OIDRolOpcion`),
  KEY `FK_fp5nbbrj0ov6gs9or3cv5h2u8` (`OIDRol`),
  CONSTRAINT `FK_dhrswql6a0tbxcvgx7ffayu2d` FOREIGN KEY (`OIDRolOpcion`) REFERENCES `rolopcion` (`OIDRolOpcion`),
  CONSTRAINT `FK_fp5nbbrj0ov6gs9or3cv5h2u8` FOREIGN KEY (`OIDRol`) REFERENCES `rol` (`OIDRol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rolrolopcion` */

insert  into `rolrolopcion`(`OIDRol`,`OIDRolOpcion`) values ('1','2');

/*Table structure for table `sistemabanco` */

DROP TABLE IF EXISTS `sistemabanco`;

CREATE TABLE `sistemabanco` (
  `OIDSistemaBanco` varchar(255) NOT NULL,
  `codigoSistemaBanco` int(11) NOT NULL,
  `fechaHabilitacionSistemaBanco` date NOT NULL,
  `fechaInhabilitacionSistemaBanco` date DEFAULT NULL,
  `OIDBanco` varchar(32) DEFAULT NULL,
  `OIDParametroConexion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDSistemaBanco`),
  UNIQUE KEY `UK_a75sm38e6fbpld17reh0a0ib4` (`codigoSistemaBanco`),
  KEY `FK_8c6cfku4ufh9frbnbqliyk2n1` (`OIDBanco`),
  KEY `FK_la4sx5kqaiisq0nvh6bvv4uwj` (`OIDParametroConexion`),
  CONSTRAINT `FK_8c6cfku4ufh9frbnbqliyk2n1` FOREIGN KEY (`OIDBanco`) REFERENCES `banco` (`OIDBanco`),
  CONSTRAINT `FK_la4sx5kqaiisq0nvh6bvv4uwj` FOREIGN KEY (`OIDParametroConexion`) REFERENCES `parametroconexion` (`OIDParametroConexion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sistemabanco` */

insert  into `sistemabanco`(`OIDSistemaBanco`,`codigoSistemaBanco`,`fechaHabilitacionSistemaBanco`,`fechaInhabilitacionSistemaBanco`,`OIDBanco`,`OIDParametroConexion`) values ('1',1,'2017-10-02',NULL,'1','1');

/*Table structure for table `tipocuenta` */

DROP TABLE IF EXISTS `tipocuenta`;

CREATE TABLE `tipocuenta` (
  `OIDTipoCuenta` varchar(32) NOT NULL,
  `fechaHabilitacionTipoCuenta` date NOT NULL,
  `fechaInhabilitacionTipoCuenta` date DEFAULT NULL,
  `codigoTipoCuenta` int(11) DEFAULT NULL,
  `nombreTipoCuenta` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDTipoCuenta`),
  UNIQUE KEY `UK_qun6o1avgpd3eik7vyl6ycnfe` (`codigoTipoCuenta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tipocuenta` */

insert  into `tipocuenta`(`OIDTipoCuenta`,`fechaHabilitacionTipoCuenta`,`fechaInhabilitacionTipoCuenta`,`codigoTipoCuenta`,`nombreTipoCuenta`) values ('1','2017-10-02',NULL,1,'Caja de ahorro');

/*Table structure for table `tipodato` */

DROP TABLE IF EXISTS `tipodato`;

CREATE TABLE `tipodato` (
  `OIDTipoDato` varchar(32) NOT NULL,
  `codTipoDato` int(11) NOT NULL,
  `nombreTipoDato` varchar(255) NOT NULL,
  `fechaHabilitacionTipoDato` date NOT NULL,
  `fechaInhabilitacionTipoDato` date DEFAULT NULL,
  PRIMARY KEY (`OIDTipoDato`),
  UNIQUE KEY `UK_prnreg6bfnobkot7veunh7v6b` (`codTipoDato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tipodato` */

insert  into `tipodato`(`OIDTipoDato`,`codTipoDato`,`nombreTipoDato`,`fechaHabilitacionTipoDato`,`fechaInhabilitacionTipoDato`) values ('1',1,'Date','2017-10-02',NULL),('2',2,'double','2017-10-02',NULL);

/*Table structure for table `tipoempresa` */

DROP TABLE IF EXISTS `tipoempresa`;

CREATE TABLE `tipoempresa` (
  `OIDTipoEmpresa` varchar(255) NOT NULL,
  `codigoTipoEmpresa` int(11) NOT NULL,
  `nombreTipoEmpresa` varchar(255) NOT NULL,
  `fechaHabTipoEmpresa` date NOT NULL,
  `fechaInhabTipoEmpresa` date DEFAULT NULL,
  PRIMARY KEY (`OIDTipoEmpresa`),
  UNIQUE KEY `UK_8jttnniahw40yobjfej8hth62` (`codigoTipoEmpresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tipoempresa` */

insert  into `tipoempresa`(`OIDTipoEmpresa`,`codigoTipoEmpresa`,`nombreTipoEmpresa`,`fechaHabTipoEmpresa`,`fechaInhabTipoEmpresa`) values ('1',1,'Servicios','2017-10-02',NULL);

/*Table structure for table `tipoimpuesto` */

DROP TABLE IF EXISTS `tipoimpuesto`;

CREATE TABLE `tipoimpuesto` (
  `OIDTipoImpuesto` varchar(255) NOT NULL,
  `codigoTipoImpuesto` int(11) NOT NULL,
  `fechaHabilitacionTipoImpuesto` date NOT NULL,
  `fechaInhabilitacionTipoImpuesto` date DEFAULT NULL,
  `modificableTipoImpuesto` bit(1) NOT NULL,
  `periodicidad` varchar(50) DEFAULT NULL,
  `nombreTipoImpuesto` varchar(50) NOT NULL,
  PRIMARY KEY (`OIDTipoImpuesto`),
  UNIQUE KEY `UK_ixsqsw5v7ck9hkj80j8a73r72` (`codigoTipoImpuesto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tipoimpuesto` */

insert  into `tipoimpuesto`(`OIDTipoImpuesto`,`codigoTipoImpuesto`,`fechaHabilitacionTipoImpuesto`,`fechaInhabilitacionTipoImpuesto`,`modificableTipoImpuesto`,`periodicidad`,`nombreTipoImpuesto`) values ('1',1,'2017-10-02',NULL,'','Mensual','Telefonia'),('2',2,'2017-10-02',NULL,'\0','Bimestral','Gas');

/*Table structure for table `tipoimpuestoatributo` */

DROP TABLE IF EXISTS `tipoimpuestoatributo`;

CREATE TABLE `tipoimpuestoatributo` (
  `OIDTipoImpuestoAtributo` varchar(32) NOT NULL,
  `codigoTipoImpuestoAtributo` int(11) NOT NULL,
  `OIDAtributo` varchar(32) DEFAULT NULL,
  `OIDTipoImpuesto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDTipoImpuestoAtributo`),
  KEY `FK_r302upy3nphgw4moutf12igk7` (`OIDAtributo`),
  KEY `FK_jh2wx2g90ti4lxwqa7gndbwfr` (`OIDTipoImpuesto`),
  CONSTRAINT `FK_jh2wx2g90ti4lxwqa7gndbwfr` FOREIGN KEY (`OIDTipoImpuesto`) REFERENCES `tipoimpuesto` (`OIDTipoImpuesto`),
  CONSTRAINT `FK_r302upy3nphgw4moutf12igk7` FOREIGN KEY (`OIDAtributo`) REFERENCES `atributo` (`OIDAtributo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tipoimpuestoatributo` */

insert  into `tipoimpuestoatributo`(`OIDTipoImpuestoAtributo`,`codigoTipoImpuestoAtributo`,`OIDAtributo`,`OIDTipoImpuesto`) values ('1',1,'1','1'),('2',2,'2','1'),('3',3,'3','1'),('4',4,'4','1'),('5',5,'1','2'),('6',6,'2','2');

/*Table structure for table `usuario` */

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `OIDUsuario` varchar(255) NOT NULL,
  `codigoUsuario` int(11) NOT NULL,
  `nombreUsuario` varchar(50) NOT NULL,
  `contraseñaUsuario` varchar(50) NOT NULL,
  `fechaHabilitacionUsuario` date NOT NULL,
  `fechaInhabilitacionUsuario` date DEFAULT NULL,
  `OIDRol` varchar(32) DEFAULT NULL,
  `OIDCliente` varchar(32) DEFAULT NULL,
  `OIDEmpresa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDUsuario`),
  UNIQUE KEY `UK_7sigp9q9rn14h113enwi0qhfg` (`codigoUsuario`),
  KEY `FK_5avoy72x3xn8a1bhmmbj74aq2` (`OIDRol`),
  KEY `FK_2x6dsb0hwjh8oksk5khfvdk6u` (`OIDCliente`),
  KEY `FK_ksggwhm49gawaj0tg8u8y68na` (`OIDEmpresa`),
  CONSTRAINT `FK_2x6dsb0hwjh8oksk5khfvdk6u` FOREIGN KEY (`OIDCliente`) REFERENCES `cliente` (`OIDCliente`),
  CONSTRAINT `FK_5avoy72x3xn8a1bhmmbj74aq2` FOREIGN KEY (`OIDRol`) REFERENCES `rol` (`OIDRol`),
  CONSTRAINT `FK_ksggwhm49gawaj0tg8u8y68na` FOREIGN KEY (`OIDEmpresa`) REFERENCES `empresa` (`OIDEmpresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `usuario` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
