-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagoimpuesto
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `atributo`
--

DROP TABLE IF EXISTS `atributo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atributo` (
  `OIDAtributo` varchar(32) NOT NULL,
  `codAtributo` int(32) NOT NULL AUTO_INCREMENT,
  `nombreAtributo` varchar(45) NOT NULL,
  `longitudAtributo` int(32) NOT NULL,
  `fechaHabAtributo` date NOT NULL,
  `fechaInhabAtributo` date DEFAULT NULL,
  `OIDTipoDato` varchar(32) NOT NULL,
  PRIMARY KEY (`OIDAtributo`),
  UNIQUE KEY `codAtributo_UNIQUE` (`codAtributo`),
  KEY `OIDTipoDato_idx` (`OIDTipoDato`),
  CONSTRAINT `OIDTipoDato` FOREIGN KEY (`OIDTipoDato`) REFERENCES `tipodato` (`OIDTipoDato`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atributo`
--

LOCK TABLES `atributo` WRITE;
/*!40000 ALTER TABLE `atributo` DISABLE KEYS */;
INSERT INTO `atributo` VALUES ('123456abcdefqrstuvwxyzghijklmnop',3,'Ganancias',8,'2012-01-01',NULL,'abcdefghijklmnopqrstuvwxyz123456'),('12345klmnopqrstuvwxyz6abcdefghij',4,'Categorizable',7,'2012-01-01',NULL,'abcdefghijklmno123456pqrstuvwxyz'),('4028808f5ef46e80015ef46ecf1a0000',9,'Test',234,'2017-10-06',NULL,'123456abcdefghijklmnopqrstuvwxyz'),('402881a65a16435b015a1644a5020004',5,'atributo probando',12,'2017-02-06',NULL,'123456abcdefghijklmnopqrstuvwxyz'),('402881a65a343270015a34330a8a0000',6,'nuevo atributo',1,'2017-02-12',NULL,'abcdefghijklmnopqrstuvwxyz123456'),('402881a65a3a6683015a3a684dc90004',7,'nuevo atrib',1,'2017-02-13',NULL,'123456abcdefghijklmnopqrstuvwxyz'),('402881a65a4a6293015a4a62c18d0000',8,'jaja',13,'2017-02-17','2017-10-06','wfghijklmnoxabcdepqrstuvyz123456'),('tuvwxyz123456abcdefghijklmnopqrs',1,'Automotor Provinciales',1,'2012-01-01',NULL,'abcdefghijklmnopqrstuvwxyz123456'),('tuvwxyz12hijklmnopqrs3456abcdefg',2,'IVA y agregado',1,'2012-01-01',NULL,'123456abcdefghijklmnopqrstuvwxyz');
/*!40000 ALTER TABLE `atributo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banco`
--

DROP TABLE IF EXISTS `banco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banco` (
  `OIDBanco` varchar(32) NOT NULL,
  `cuitBanco` varchar(11) NOT NULL,
  `nombreBanco` varchar(45) NOT NULL,
  `fechaHabilitacionBanco` date NOT NULL,
  `fechaInhabilitacionBanco` date DEFAULT NULL,
  PRIMARY KEY (`OIDBanco`),
  UNIQUE KEY `OIDBanco_UNIQUE` (`OIDBanco`),
  UNIQUE KEY `cuitBanco_UNIQUE` (`cuitBanco`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banco`
--

LOCK TABLES `banco` WRITE;
/*!40000 ALTER TABLE `banco` DISABLE KEYS */;
INSERT INTO `banco` VALUES ('402880825e886d58015e886d59ba0000','1','Santander 2','2017-09-15',NULL),('402880825e901305015e9014014a0000','11','Macro','2017-09-17','2017-09-17'),('402880825e9031f2015e90325df50000','2','Banco Test','2017-09-17','2017-09-17'),('402880825ea74c77015ea74ca3c00000','33','Macro','2017-09-21',NULL),('4028808f5ef468ec015ef4699cc30000','4','Galicia','2017-10-06','2017-10-06');
/*!40000 ALTER TABLE `banco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comision`
--

DROP TABLE IF EXISTS `comision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comision` (
  `OIDComision` varchar(32) NOT NULL,
  `valorComision` int(2) NOT NULL,
  PRIMARY KEY (`OIDComision`),
  UNIQUE KEY `OIDComision_UNIQUE` (`OIDComision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comision`
--

LOCK TABLES `comision` WRITE;
/*!40000 ALTER TABLE `comision` DISABLE KEYS */;
INSERT INTO `comision` VALUES ('123456abcdefghijklmnopqrstuvwxyz',35),('abcdefghijklmno123456pqrstuvwxyz',22),('abcdefghijklmnopqrstuvwxyz123456',19),('abcdefghijklmnoxyz123456pqrstuvw',46),('abcdepqrstuvwfghijklmnoxyz123456',59),('wfghijklmnoxabcdepqrstuvyz123456',76);
/*!40000 ALTER TABLE `comision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `OIDEmpresa` varchar(32) NOT NULL,
  `cuitEmpresa` varchar(11) NOT NULL,
  `nombreEmpresa` varchar(32) NOT NULL,
  `codTipoEmpresa` int(11) NOT NULL,
  `fechaHabilitacionEmpresa` date NOT NULL,
  `fechaInhabilitacionEmpresa` date DEFAULT NULL,
  PRIMARY KEY (`OIDEmpresa`),
  UNIQUE KEY `OIDEmpresa_UNIQUE` (`OIDEmpresa`),
  UNIQUE KEY `cuitEmpresa_UNIQUE` (`cuitEmpresa`),
  KEY `codTipoEmpresa` (`codTipoEmpresa`),
  CONSTRAINT `codTipoEmpresa` FOREIGN KEY (`codTipoEmpresa`) REFERENCES `tipoempresa` (`codTipoEmpresa`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresatipoimpuesto`
--

DROP TABLE IF EXISTS `empresatipoimpuesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresatipoimpuesto` (
  `OIDEmpresaTipoImpuesto` varchar(32) NOT NULL,
  `codigoEmpresaTipoImpuesto` int(11) NOT NULL AUTO_INCREMENT,
  `fechaHabilitacionEmpresaSistema` date NOT NULL,
  `fechaInhabilitacionEmpresaSistema` date DEFAULT NULL,
  `periodoLiquidacion` varchar(32) NOT NULL,
  `urlComprobante` varchar(32) NOT NULL,
  `urlEmpresaTipoImpuesto` varchar(32) NOT NULL,
  `OIDEmpresa` varchar(32) NOT NULL,
  `OIDTipoImpuesto` varchar(32) NOT NULL,
  PRIMARY KEY (`OIDEmpresaTipoImpuesto`),
  UNIQUE KEY `OIDEmpresaTipoImpuesto_UNIQUE` (`OIDEmpresaTipoImpuesto`),
  UNIQUE KEY `codigoEmpresaTipoImpuesto_UNIQUE` (`codigoEmpresaTipoImpuesto`),
  KEY `OIDEmpresa` (`OIDEmpresa`),
  CONSTRAINT `OIDEmpresa` FOREIGN KEY (`OIDEmpresa`) REFERENCES `empresa` (`OIDEmpresa`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresatipoimpuesto`
--

LOCK TABLES `empresatipoimpuesto` WRITE;
/*!40000 ALTER TABLE `empresatipoimpuesto` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresatipoimpuesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametrocomision`
--

DROP TABLE IF EXISTS `parametrocomision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametrocomision` (
  `OIDParametroComision` varchar(32) NOT NULL,
  `codigoParametroComision` varchar(11) NOT NULL,
  `porcentajeParametroComision` int(11) NOT NULL,
  `fechaHabilitacionParametroComision` date NOT NULL,
  `fechaInhabilitacionParametroComision` date DEFAULT NULL,
  PRIMARY KEY (`OIDParametroComision`),
  UNIQUE KEY `OIDParametroComision_UNIQUE` (`OIDParametroComision`),
  UNIQUE KEY `codigoParametroComision_UNIQUE` (`codigoParametroComision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametrocomision`
--

LOCK TABLES `parametrocomision` WRITE;
/*!40000 ALTER TABLE `parametrocomision` DISABLE KEYS */;
/*!40000 ALTER TABLE `parametrocomision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametroporeditable`
--

DROP TABLE IF EXISTS `parametroporeditable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametroporeditable` (
  `OIDParametroPorEditable` varchar(32) NOT NULL,
  `porcentajeCon` int(11) NOT NULL,
  `porcentajeSin` int(11) NOT NULL,
  PRIMARY KEY (`OIDParametroPorEditable`),
  UNIQUE KEY `OIDParametroPorEditable_UNIQUE` (`OIDParametroPorEditable`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametroporeditable`
--

LOCK TABLES `parametroporeditable` WRITE;
/*!40000 ALTER TABLE `parametroporeditable` DISABLE KEYS */;
/*!40000 ALTER TABLE `parametroporeditable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametroporperiodicidad`
--

DROP TABLE IF EXISTS `parametroporperiodicidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametroporperiodicidad` (
  `OIDParametroPorPeriodicidad` varchar(32) NOT NULL,
  `porcentajeAnual` int(11) NOT NULL,
  `porcentajeBimestral` int(11) NOT NULL,
  `porcentajeMensual` int(11) NOT NULL,
  `porcentajeTrimestral` int(11) NOT NULL,
  PRIMARY KEY (`OIDParametroPorPeriodicidad`),
  UNIQUE KEY `OIDParametroPorPeriodicidad_UNIQUE` (`OIDParametroPorPeriodicidad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametroporperiodicidad`
--

LOCK TABLES `parametroporperiodicidad` WRITE;
/*!40000 ALTER TABLE `parametroporperiodicidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `parametroporperiodicidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipodato`
--

DROP TABLE IF EXISTS `tipodato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodato` (
  `OIDTipoDato` varchar(32) NOT NULL,
  `codTipoDato` int(11) NOT NULL AUTO_INCREMENT,
  `nombreTipoDato` varchar(45) NOT NULL,
  `fechaHabTipoDato` date NOT NULL,
  `fechaInhabTipoDato` date DEFAULT NULL,
  PRIMARY KEY (`OIDTipoDato`),
  UNIQUE KEY `OIDTipoDato_UNIQUE` (`OIDTipoDato`),
  UNIQUE KEY `codTipoDato_UNIQUE` (`codTipoDato`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipodato`
--

LOCK TABLES `tipodato` WRITE;
/*!40000 ALTER TABLE `tipodato` DISABLE KEYS */;
INSERT INTO `tipodato` VALUES ('123456abcdefghijklmnopqrstuvwxyz',3,'Cadena','2014-04-05',NULL),('abcdefghijklmno123456pqrstuvwxyz',2,'Real','2013-02-02',NULL),('abcdefghijklmnopqrstuvwxyz123456',1,'Entero','2013-02-02',NULL),('abcdefghijklmnoxyz123456pqrstuvw',4,'Estructura','2012-01-06',NULL),('abcdepqrstuvwfghijklmnoxyz123456',5,'Tipo Dato de Prueba','2013-01-01','2014-03-05'),('wfghijklmnoxabcdepqrstuvyz123456',6,'nueva prueba','2013-02-03',NULL);
/*!40000 ALTER TABLE `tipodato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoempresa`
--

DROP TABLE IF EXISTS `tipoempresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoempresa` (
  `OIDTipoEmpresa` varchar(32) NOT NULL,
  `codTipoEmpresa` int(11) NOT NULL AUTO_INCREMENT,
  `nombreTipoEmpresa` varchar(45) NOT NULL,
  `fechaHabTipoEmpresa` date NOT NULL,
  `fechaInhabTipoEmpresa` date DEFAULT NULL,
  PRIMARY KEY (`OIDTipoEmpresa`),
  UNIQUE KEY `OIDTipoEmpresa_UNIQUE` (`OIDTipoEmpresa`),
  UNIQUE KEY `codTipoEmpresa_UNIQUE` (`codTipoEmpresa`)
) ENGINE=InnoDB AUTO_INCREMENT=2324 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoempresa`
--

LOCK TABLES `tipoempresa` WRITE;
/*!40000 ALTER TABLE `tipoempresa` DISABLE KEYS */;
INSERT INTO `tipoempresa` VALUES ('3456abcdefghijklmnopqrstuvwxyz',1,'Servicios','2014-04-05','2017-04-10');
/*!40000 ALTER TABLE `tipoempresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoimpuesto`
--

DROP TABLE IF EXISTS `tipoimpuesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoimpuesto` (
  `OIDTipoImpuesto` varchar(32) NOT NULL,
  `codTipoImpuesto` int(32) NOT NULL AUTO_INCREMENT,
  `nombreTipoImpuesto` varchar(45) NOT NULL,
  `modificableTipoImpuesto` int(1) NOT NULL,
  `fechaHabTipoImpuesto` date NOT NULL,
  `fechaInhabTipoImpuesto` date DEFAULT NULL,
  PRIMARY KEY (`OIDTipoImpuesto`),
  UNIQUE KEY `codTipoImpuesto_UNIQUE` (`codTipoImpuesto`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoimpuesto`
--

LOCK TABLES `tipoimpuesto` WRITE;
/*!40000 ALTER TABLE `tipoimpuesto` DISABLE KEYS */;
INSERT INTO `tipoimpuesto` VALUES ('402881a65a1641dd015a164280d00000',4,'impuesto 4',0,'2017-02-13',NULL),('402881a65a164697015a1647927d0000',6,'jaja',0,'2017-02-13',NULL),('402881a65a164697015a16492a990003',2,'otro tipo impuesto',0,'2017-02-13',NULL),('402881a65a164697015a164ba7280009',5,'impuesto prueba',0,'2017-02-13',NULL),('402881a65a2f50f8015a2f51ba210000',7,'p',0,'2017-02-13',NULL),('402881a65a2f60bc015a2f60d0210000',8,'a',0,'2017-02-11',NULL),('402881a65a343270015a3433942f0001',9,'nuevo ti',0,'2017-02-13',NULL),('402881a65a36681c015a366869130000',1,'viejo tipo impuesto',0,'2017-02-06',NULL),('402881a65a383316015a38334b550000',3,'tipo impuesto 3',1,'2017-02-13',NULL),('402881a65a3a6683015a3a66c1af0000',11,'perro',0,'2017-02-13',NULL),('ff8080815a39e618015a39e7dfb30003',10,'Agua',0,'2017-02-13',NULL);
/*!40000 ALTER TABLE `tipoimpuesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoimpuestoatributo`
--

DROP TABLE IF EXISTS `tipoimpuestoatributo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoimpuestoatributo` (
  `OIDTipoImpuestoAtributo` varchar(32) NOT NULL,
  `nroOrden` int(11) NOT NULL,
  `OIDAtributo` varchar(32) NOT NULL,
  `OIDTipoImpuesto` varchar(32) NOT NULL,
  PRIMARY KEY (`OIDTipoImpuestoAtributo`),
  KEY `OIDAtributo_idx` (`OIDAtributo`),
  KEY `OIDTipoImpuesto_idx` (`OIDTipoImpuesto`),
  CONSTRAINT `OIDAtributo` FOREIGN KEY (`OIDAtributo`) REFERENCES `atributo` (`OIDAtributo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `OIDTipoImpuesto` FOREIGN KEY (`OIDTipoImpuesto`) REFERENCES `tipoimpuesto` (`OIDTipoImpuesto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoimpuestoatributo`
--

LOCK TABLES `tipoimpuestoatributo` WRITE;
/*!40000 ALTER TABLE `tipoimpuestoatributo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipoimpuestoatributo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `OIDUsuario` varchar(32) NOT NULL,
  `codigoUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombreUsuario` varchar(50) NOT NULL,
  `contraseñaUsuario` varchar(50) NOT NULL,
  PRIMARY KEY (`OIDUsuario`),
  UNIQUE KEY `OIDUsuario_UNIQUE` (`OIDUsuario`),
  UNIQUE KEY `codigoUsuario_UNIQUE` (`codigoUsuario`),
  UNIQUE KEY `nombreUsuario_UNIQUE` (`nombreUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES ('abcdefg1234567-hij890-abcde12331',8,'muñoz','lorenzo');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-07  3:06:25
