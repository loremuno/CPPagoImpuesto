/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CP;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
/**
 *
 * @author Lorenzo
 */
public class Ejecutar {

    /**
     * @param args the command line arguments
     */
    public boolean casoPrueba(String casoDeUso,String cp) throws IOException {
        try {
            String url = "jdbc:mysql://localhost:3306/PagoImpuesto";
            Connection conn = DriverManager.getConnection(url,"root","root");
            ScriptRunner runner = new ScriptRunner(conn, true, true);
            runner.runScript(new BufferedReader(new FileReader(casoDeUso+"\\"+cp+".sql")));
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return true;
        }
    }
}