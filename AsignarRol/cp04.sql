
use pagoimpuesto;
SET foreign_key_checks = 0 ; SET SQL_SAFE_UPDATES = 0;

--
-- Base de datos: `pagoimpuesto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `atributo`
--

delete from `atributo` ;


--
-- Volcado de datos para la tabla `atributo`
--

INSERT INTO `atributo` (`OIDAtributo`, `codigoAtributo`, `fechaHabilitacionAtributo`, `fechaInhabilitacionAtributo`, `longitudAtributo`, `nombreAtributo`, `OIDTipoDato`) VALUES
('1', 1, '2017-10-02', NULL, NULL, 'Importe', '2'),
('2', 2, '2017-10-02', NULL, NULL, 'Fecha de vencimiento', '1'),
('3', 3, '2017-10-02', NULL, NULL, 'Fecha proximo vencimiento', '1'),
('4', 4, '2017-10-02', NULL, NULL, 'Monto minimo exigible', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco`
--

delete from `banco` ;

--
-- Volcado de datos para la tabla `banco`
--

INSERT INTO `banco` (`OIDBanco`, `fechaHabilitacionBanco`, `fechaInhabilitacionBanco`, `cuitBanco`, `nombreBanco`) VALUES
('1', '2017-10-02', NULL, '1', 'Banco Nacion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

delete from `cliente` ;
--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`OIDCliente`, `apellidoCliente`, `dniCliente`, `nombreCliente`, `numeroCliente`, `fechaHabilitacionCliente`, `fechaInhabilitacionCliente`) VALUES
('1', 'Garay', 36731121, 'Fernando', '1', '2017-10-02', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comision`
--

delete from `comision` ;

--
-- Volcado de datos para la tabla `comision`
--

INSERT INTO `comision` (`OIDComision`, `valorcomision`, `porcentajeUtilizado`, `OIDOperacion`, `OIDComisionEmpresaSistema`) VALUES
('402881e95f0401c6015f0401c8a00002', 2.5, 0.5, '402881e95f035428015f035459160000', '402881e95f0401c6015f0401c89c0000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comisionempresasistema`
--

delete from `comisionempresasistema` ;
--
-- Volcado de datos para la tabla `comisionempresasistema`
--

INSERT INTO `comisionempresasistema` (`OIDComisionEmpresaSistema`, `codigoces`, `fechacomisionempresasistema`, `OIDEmpresaTipoImpuesto`, `periodicidadUtilizada`) VALUES
('402881e95f0401c6015f0401c89c0000', 1, '2017-10-09', '2', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comisionempresasistemaestado`
--

delete from `comisionempresasistemaestado` ;
--
-- Volcado de datos para la tabla `comisionempresasistemaestado`
--

INSERT INTO `comisionempresasistemaestado` (`OIDComisionEmpresaSistemaEstado`, `codigoces`, `nombreEstadoCES`) VALUES
('1', 1, 'Pendiente'),
('2', 2, 'Anulada'),
('3', 3, 'Confirmada'),
('4', 4, 'Recalculado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

delete from `cuenta` ;

--
-- Volcado de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`OIDCuenta`, `fechaHabilitacionCuenta`, `fechaInhabilitacionCuenta`, `numeroCuenta`, `OIDTipoCuenta`, `OIDCliente`, `OIDBanco`) VALUES
('1', '2017-10-02', NULL, 1, '1', '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

delete from `empresa` ;
--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`OIDEmpresa`, `cuitEmpresa`, `fechaHabilitacionEmpresa`, `fechaInhabilitacionEmpresa`, `nombreEmpresa`, `OIDTipoEmpresa`) VALUES
('1', '1', '2017-10-02', NULL, 'Claro', '1'),
('2', '2', '2017-10-02', NULL, 'ECOGas', '1'),
('3', '3', '2017-10-02', NULL, 'Movistar', '1'),
('402881e95f306bae015f306c25c20000', '112233', '2017-10-18', NULL, 'MaxiSA', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresatipoimpuesto`
--

delete from `empresatipoimpuesto` ;
--
-- Volcado de datos para la tabla `empresatipoimpuesto`
--

INSERT INTO `empresatipoimpuesto` (`OIDEmpresaTipoImpuesto`, `codigoempresatipoimpuesto`, `periodoLiquidacion`, `fechaInhabilitacionEmpresaTipoImpuesto`, `fechaHabilitacionEmpresaTipoImpuesto`, `OIDTipoImpuesto`, `OIDEmpresa`) VALUES
('1', 1, 1, NULL, '2017-10-02', '1', '1'),
('2', 2, 2, NULL, '2017-08-01', '2', '2'),
('3', 3, 1, NULL, '2017-10-02', '1', '3'),
('402881e95f30af78015f30afce300000', 4, 1, '2017-10-18', '2017-10-18', '1', '402881e95f306bae015f306c25c20000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresatipoimpuestoatributo`
--

delete from `empresatipoimpuestoatributo` ;
--
-- Volcado de datos para la tabla `empresatipoimpuestoatributo`
--

INSERT INTO `empresatipoimpuestoatributo` (`OIDEmpresaTipoImpuestoAtributo`, `orden`, `formato`, `valorPeriodicidad`, `OIDTipoImpuestoAtributo`, `OIDEmpresaTipoImpuesto`) VALUES
('1', 0, NULL, NULL, '1', '1'),
('10', 3, NULL, NULL, '4', '3'),
('2', 1, NULL, NULL, '2', '1'),
('3', 2, NULL, NULL, '3', '1'),
('4', 3, NULL, NULL, '4', '1'),
('402881e95f313d39015f313dbcfc0000', 0, NULL, NULL, '1', '402881e95f30af78015f30afce300000'),
('402881e95f313d39015f313dbd020001', 1, NULL, NULL, '2', '402881e95f30af78015f30afce300000'),
('402881e95f313d39015f313dbd050002', 2, NULL, NULL, '3', '402881e95f30af78015f30afce300000'),
('5', 0, NULL, NULL, '5', '2'),
('6', 1, NULL, NULL, '6', '2'),
('7', 0, NULL, NULL, '1', '3'),
('8', 1, NULL, NULL, '2', '3'),
('9', 2, NULL, NULL, '3', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadocomisionempresasistema`
--

delete from `estadocomisionempresasistema` ;

--
-- Volcado de datos para la tabla `estadocomisionempresasistema`
--

INSERT INTO `estadocomisionempresasistema` (`OIDEstadoComisionEmpresaSistema`, `fechaestadocesdesde`, `fechaestadoceshasta`, `OIDComisionEmpresaSistemaEstado`, `OIDComisionEmpresaSistema`) VALUES
('402881e95f0401c6015f0401c8a00001', '2017-10-09', '2017-10-09', '1', '402881e95f0401c6015f0401c89c0000'),
('402881e95f0401fb015f04026ad60000', '2017-10-09', NULL, '3', '402881e95f0401c6015f0401c89c0000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operacion`
--

delete from `operacion` ;

--
-- Volcado de datos para la tabla `operacion`
--

INSERT INTO `operacion` (`OIDOperacion`, `codpagoelect`, `fechavencimientocomprobanteimpago`, `fechaoperacion`, `importeOperacion`, `numeroComprobante`, `ComisionCalculadaOperacion`, `numerooperacion`, `OIDTipoImpuesto`, `OIDEmpresaTipoImpuesto`, `OIDCuenta`, `OIDCliente`) VALUES
('402881e95f035428015f035459160000', '123', '2017-10-09', '2017-08-01', 500, 1, b'1', 1, '2', '2', '1', '1'),
('402881e95f03fabf015f03fb07f00000', '123', '2017-10-09', '2017-10-09', 500, 1, b'0', 2, '2', '2', '1', '1'),
('402881e95f32681f015f326866770000', '123', '2018-06-06', '2017-10-18', 100, 1, b'0', 3, '1', '1', '1', '1'),
('402881e95f32681f015f32689fa70002', '123', '0018-06-09', '2017-10-18', 100, 1, b'0', 4, '2', '2', '1', '1'),
('402881e95f32681f015f3268f2020003', '123', '0018-06-09', '2017-10-18', 300, 3, b'0', 5, '1', '3', '1', '1'),
('402881e95f327768015f327b459a0000', '123', '0018-06-09', '2017-10-18', 50, 1, b'0', 6, '1', '3', '1', '1'),
('402881e95f32a72e015f32a780ab0000', '123', '0018-06-09', '2017-10-19', 45, 3, b'0', 7, '1', '1', '1', '1'),
('402881e95f32bbec015f32bcb1920000', '123', '2017-12-12', '2017-10-19', 200, 2, b'0', 8, '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operacionatributo`
--

delete from `operacionatributo`;
--
-- Volcado de datos para la tabla `operacionatributo`
--

INSERT INTO `operacionatributo` (`OIDOperacionAtributo`, `valorOperacionAtibuto`, `OIDTipoImpuestoAtributo`, `OIDOperacion`) VALUES
('402881e95f32681f015f3268667f0001', '50.0', '3', '402881e95f32681f015f326866770000'),
('402881e95f32681f015f3268f2020004', '120.0', '4', '402881e95f32681f015f3268f2020003'),
('402881e95f327768015f327b459f0001', '50.0', '4', '402881e95f327768015f327b459a0000'),
('402881e95f32a72e015f32a780b30001', '2018-01-01T00:00:00-03:00', '3', '402881e95f32a72e015f32a780ab0000'),
('402881e95f32a72e015f32a780b30002', '45.0', '4', '402881e95f32a72e015f32a780ab0000'),
('402881e95f32bbec015f32bcb1a20001', '2018-01-01T00:00:00-03:00', '3', '402881e95f32bbec015f32bcb1920000'),
('402881e95f32bbec015f32bcb1a20002', '20.0', '4', '402881e95f32bbec015f32bcb1920000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametroconexion`
--

delete from `parametroconexion` ;
--
-- Volcado de datos para la tabla `parametroconexion`
--

INSERT INTO `parametroconexion` (`OIDParametroConexion`, `codigoConexion`, `codigoParametroConexion`, `fechaInhabilitacionParametroConexion`, `fechaHabilitacionParametroConexion`) VALUES
('1', '1', 1, NULL, '2017-10-02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametroporeditable`
--

delete from `parametroporeditable` ;

--
-- Volcado de datos para la tabla `parametroporeditable`
--

INSERT INTO `parametroporeditable` (`OIDParametroPorEditable`, `porcentajecon`, `porcentajesin`) VALUES
('402881e95f352ad2015f352aff0e0000', 1.2, 3.5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametroporperiodicidad`
--

delete from `parametroporperiodicidad` ;
--
-- Volcado de datos para la tabla `parametroporperiodicidad`
--

INSERT INTO `parametroporperiodicidad` (`OIDParametroPorPeriodicidad`, `porcentajeanual`, `porcentajemensual`, `porcentajebimestral`, `porcentajetrimestral`) VALUES
('1', 0.5, 0.5, 0.5, 0.5),
('402881e95f356fad015f356fd9ae0000', 2.1, 6.6, 1.1, 3.4),
('402881e95f35705b015f357099650000', 5.8, 10.8, 7.9, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

delete from `rol` ;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`OIDRol`, `fechaHabilitacionRol`, `fechaInhabilitacionRol`, `codigoRol`, `nombreRol`) VALUES
('1', '2017-10-02', NULL, 1, 'ClienteBanco'),
('2', '2017-10-02', NULL, 2, 'ClienteEmpresa'),
('3', '2017-10-19', NULL, 3, 'Admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rolopcion`
--

delete from `rolopcion`;

--
-- Volcado de datos para la tabla `rolopcion`
--

INSERT INTO `rolopcion` (`OIDRolOpcion`, `fechaHabilitacionRolOpcion`, `fechaInhabilitacionRolOpcion`, `codigoRolOpcion`, `nombreRolOpcion`) VALUES
('1', '2017-10-02', NULL, 1, 'CUPAGARIMPUESTO'),
('10', '2017-10-02', NULL, 10, 'CUABMPARAMETROPORPERIODICIDAD'),
('2', '2017-10-02', NULL, 2, 'CUCONSULTAROPERACIONES'),
('3', '2017-10-02', NULL, 3, 'CUEXPORTAROPERACIONES'),
('4', '2017-10-02', NULL, 4, 'CUCONFIRMARANULARCOMISION'),
('402881e95f3068d4015f306910df0000', '2017-10-18', NULL, 7, 'CUABMEMPRESA'),
('402881e95f3504db015f350575310000', '2017-10-19', NULL, 9, 'CUABMPARAMETROPOREDITABLE'),
('5', '2017-10-02', NULL, 5, 'CUABMTIPOIMPUESTO'),
('6', '2017-10-02', NULL, 6, 'CUABMROLOPCION'),
('8', '2017-10-02', NULL, 8, 'CUABMROL');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rolrolopcion`
--

delete from `rolrolopcion` ;

--
-- Volcado de datos para la tabla `rolrolopcion`
--

INSERT INTO `rolrolopcion` (`OIDRol`, `OIDRolOpcion`) VALUES
('1', '1'),
('1', '2'),
('2', '3'),
('3', '1'),
('3', '2'),
('3', '3'),
('3', '4'),
('3', '402881e95f3068d4015f306910df0000'),
('3', '402881e95f3504db015f350575310000'),
('3', '5'),
('3', '6'),
('3', '8'),
('3', '10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sistemabanco`
--

delete from `sistemabanco`;
  
--
-- Volcado de datos para la tabla `sistemabanco`
--

INSERT INTO `sistemabanco` (`OIDSistemaBanco`, `codigoSistemaBanco`, `fechaHabilitacionSistemaBanco`, `fechaInhabilitacionSistemaBanco`, `OIDBanco`, `OIDParametroConexion`) VALUES
('1', 1, '2017-10-02', NULL, '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipocuenta`
--

delete from `tipocuenta` ;


--
-- Volcado de datos para la tabla `tipocuenta`
--

INSERT INTO `tipocuenta` (`OIDTipoCuenta`, `fechaHabilitacionTipoCuenta`, `fechaInhabilitacionTipoCuenta`, `codigoTipoCuenta`, `nombreTipoCuenta`) VALUES
('1', '2017-10-02', NULL, 1, 'Caja de ahorro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodato`
--

delete from `tipodato`;


--
-- Volcado de datos para la tabla `tipodato`
--

INSERT INTO `tipodato` (`OIDTipoDato`, `codTipoDato`, `nombreTipoDato`, `fechaHabilitacionTipoDato`, `fechaInhabilitacionTipoDato`) VALUES
('1', 1, 'Date', '2017-10-02', NULL),
('2', 2, 'double', '2017-10-02', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoempresa`
--

delete from `tipoempresa`;


--
-- Volcado de datos para la tabla `tipoempresa`
--

INSERT INTO `tipoempresa` (`OIDTipoEmpresa`, `codigoTipoEmpresa`, `nombreTipoEmpresa`, `fechaHabTipoEmpresa`, `fechaInhabTipoEmpresa`) VALUES
('1', 1, 'Servicios', '2017-10-02', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoimpuesto`
--

delete from `tipoimpuesto`;
 

--
-- Volcado de datos para la tabla `tipoimpuesto`
--

INSERT INTO `tipoimpuesto` (`OIDTipoImpuesto`, `codigoTipoImpuesto`, `fechaHabilitacionTipoImpuesto`, `fechaInhabilitacionTipoImpuesto`, `modificableTipoImpuesto`, `periodicidad`, `nombreTipoImpuesto`) VALUES
('1', 1, '2017-10-02', NULL, b'1', 'Mensual', 'Telefonia'),
('2', 2, '2017-10-02', NULL, b'0', 'Bimestral', 'Gas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoimpuestoatributo`
--

delete from `tipoimpuestoatributo`;


--
-- Volcado de datos para la tabla `tipoimpuestoatributo`
--

INSERT INTO `tipoimpuestoatributo` (`OIDTipoImpuestoAtributo`, `codigoTipoImpuestoAtributo`, `OIDAtributo`, `OIDTipoImpuesto`) VALUES
('1', 1, '1', '1'),
('2', 2, '2', '1'),
('3', 3, '3', '1'),
('4', 4, '4', '1'),
('5', 5, '1', '2'),
('6', 6, '2', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

delete from `usuario`;
 
--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`OIDUsuario`, `codigoUsuario`, `nombreUsuario`, `contraseñaUsuario`, `fechaHabilitacionUsuario`, `fechaInhabilitacionUsuario`, `OIDRol`, `OIDCliente`, `OIDEmpresa`) VALUES

('3', 3, 'Admin', 'admin', '2017-10-02', NULL, '3', NULL, NULL);

SET foreign_key_checks = 1 ; SET SQL_SAFE_UPDATES = 1;
--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `atributo`
--
