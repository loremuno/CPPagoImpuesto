-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagoimpuesto
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `atributo`
--

DROP TABLE IF EXISTS `atributo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atributo` (
  `OIDAtributo` varchar(32) NOT NULL,
  `codigoAtributo` int(11) NOT NULL,
  `fechaHabilitacionAtributo` date NOT NULL,
  `fechaInhabilitacionAtributo` date DEFAULT NULL,
  `longitudAtributo` int(11) DEFAULT NULL,
  `nombreAtributo` varchar(45) NOT NULL,
  `OIDTipoDato` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`OIDAtributo`),
  UNIQUE KEY `UK_d0yib8j696bhxicp6187olcbn` (`codigoAtributo`),
  KEY `FK_bbgko1mw0ucuoga8wiovkxj75` (`OIDTipoDato`),
  CONSTRAINT `FK_bbgko1mw0ucuoga8wiovkxj75` FOREIGN KEY (`OIDTipoDato`) REFERENCES `tipodato` (`OIDTipoDato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atributo`
--

LOCK TABLES `atributo` WRITE;
/*!40000 ALTER TABLE `atributo` DISABLE KEYS */;
INSERT INTO `atributo` VALUES ('1',1,'2017-10-02',NULL,NULL,'Importe','2'),('2',2,'2017-10-02',NULL,NULL,'Fecha de vencimiento','1'),('3',3,'2017-10-02',NULL,NULL,'Fecha Proximo vencimiento','1'),('4',4,'2017-10-02',NULL,NULL,'Monto minimo exigible','2');
/*!40000 ALTER TABLE `atributo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banco`
--

DROP TABLE IF EXISTS `banco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banco` (
  `OIDBanco` varchar(32) NOT NULL,
  `fechaHabilitacionBanco` date DEFAULT NULL,
  `fechaInhabilitacionBanco` date DEFAULT NULL,
  `cuitBanco` varchar(255) DEFAULT NULL,
  `nombreBanco` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDBanco`),
  UNIQUE KEY `UK_1c70fmn989x0w3x3oe6kh3xex` (`cuitBanco`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banco`
--

LOCK TABLES `banco` WRITE;
/*!40000 ALTER TABLE `banco` DISABLE KEYS */;
INSERT INTO `banco` VALUES ('1','2017-10-02',NULL,'1','Banco Nacion'),('2','2017-08-10',NULL,'2','Banco Galicia');
/*!40000 ALTER TABLE `banco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `OIDCliente` varchar(32) NOT NULL,
  `apellidoCliente` varchar(255) NOT NULL,
  `dniCliente` int(11) NOT NULL,
  `nombreCliente` varchar(255) NOT NULL,
  `numeroCliente` varchar(255) NOT NULL,
  `fechaHabilitacionCliente` date DEFAULT NULL,
  `fechaInhabilitacionCliente` date DEFAULT NULL,
  PRIMARY KEY (`OIDCliente`),
  UNIQUE KEY `UK_spjnf3pklvp1902gxxbhksaob` (`numeroCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES ('1','Garay',36731121,'Fernando','1','2017-10-02',NULL);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comision`
--

DROP TABLE IF EXISTS `comision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comision` (
  `OIDComision` varchar(255) NOT NULL,
  `valorcomision` double NOT NULL,
  `porcentajeUtilizado` double NOT NULL,
  `OIDOperacion` varchar(255) DEFAULT NULL,
  `OIDComisionEmpresaSistema` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDComision`),
  KEY `FK_7o63e1rj32iyd5ohqmj8lfx61` (`OIDOperacion`),
  KEY `FK_myhupess14vq3vgqhw7xcu7an` (`OIDComisionEmpresaSistema`),
  CONSTRAINT `FK_7o63e1rj32iyd5ohqmj8lfx61` FOREIGN KEY (`OIDOperacion`) REFERENCES `operacion` (`OIDOperacion`),
  CONSTRAINT `FK_myhupess14vq3vgqhw7xcu7an` FOREIGN KEY (`OIDComisionEmpresaSistema`) REFERENCES `comisionempresasistema` (`OIDComisionEmpresaSistema`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comision`
--

LOCK TABLES `comision` WRITE;
/*!40000 ALTER TABLE `comision` DISABLE KEYS */;
/*!40000 ALTER TABLE `comision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comisionempresasistema`
--

DROP TABLE IF EXISTS `comisionempresasistema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comisionempresasistema` (
  `OIDComisionEmpresaSistema` varchar(255) NOT NULL,
  `codigoces` int(11) NOT NULL,
  `fechacomisionempresasistema` date NOT NULL,
  `OIDEmpresaTipoImpuesto` varchar(255) DEFAULT NULL,
  `periodicidadUtilizada` int(11) NOT NULL,
  PRIMARY KEY (`OIDComisionEmpresaSistema`),
  KEY `FK_8kqpv80oul55hms4gjvsi302` (`OIDEmpresaTipoImpuesto`),
  CONSTRAINT `FK_8kqpv80oul55hms4gjvsi302` FOREIGN KEY (`OIDEmpresaTipoImpuesto`) REFERENCES `empresatipoimpuesto` (`OIDEmpresaTipoImpuesto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comisionempresasistema`
--

LOCK TABLES `comisionempresasistema` WRITE;
/*!40000 ALTER TABLE `comisionempresasistema` DISABLE KEYS */;
INSERT INTO `comisionempresasistema` VALUES ('402881e95f0401c6015f0401c89c0000',1,'2017-10-09','2',2);
/*!40000 ALTER TABLE `comisionempresasistema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comisionempresasistemaestado`
--

DROP TABLE IF EXISTS `comisionempresasistemaestado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comisionempresasistemaestado` (
  `OIDComisionEmpresaSistemaEstado` varchar(255) NOT NULL,
  `codigoces` int(11) NOT NULL,
  `nombreEstadoCES` varchar(255) NOT NULL,
  PRIMARY KEY (`OIDComisionEmpresaSistemaEstado`),
  UNIQUE KEY `UK_lijot92p9rcrgrdlgibbf58u7` (`codigoces`),
  UNIQUE KEY `UK_3a63e5ekeqx7l0p0khd3w1cw4` (`nombreEstadoCES`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comisionempresasistemaestado`
--

LOCK TABLES `comisionempresasistemaestado` WRITE;
/*!40000 ALTER TABLE `comisionempresasistemaestado` DISABLE KEYS */;
INSERT INTO `comisionempresasistemaestado` VALUES ('1',1,'Pendiente'),('2',2,'Anulada'),('3',3,'Confirmada'),('4',4,'Recalculado');
/*!40000 ALTER TABLE `comisionempresasistemaestado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuenta`
--

DROP TABLE IF EXISTS `cuenta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuenta` (
  `OIDCuenta` varchar(32) NOT NULL,
  `fechaHabilitacionCuenta` date DEFAULT NULL,
  `fechaInhabilitacionCuenta` date DEFAULT NULL,
  `numeroCuenta` int(11) DEFAULT NULL,
  `OIDTipoCuenta` varchar(32) DEFAULT NULL,
  `OIDCliente` varchar(32) DEFAULT NULL,
  `OIDBanco` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`OIDCuenta`),
  KEY `FK_nsw6u6h0f2y7yrcxq99venu5y` (`OIDTipoCuenta`),
  KEY `FK_3qxiemyc9uaws4aw8mbe9iy9c` (`OIDCliente`),
  KEY `FK_psr41x6qd7t1m3i7jy4p6qx2s` (`OIDBanco`),
  CONSTRAINT `FK_3qxiemyc9uaws4aw8mbe9iy9c` FOREIGN KEY (`OIDCliente`) REFERENCES `cliente` (`OIDCliente`),
  CONSTRAINT `FK_nsw6u6h0f2y7yrcxq99venu5y` FOREIGN KEY (`OIDTipoCuenta`) REFERENCES `tipocuenta` (`OIDTipoCuenta`),
  CONSTRAINT `FK_psr41x6qd7t1m3i7jy4p6qx2s` FOREIGN KEY (`OIDBanco`) REFERENCES `banco` (`OIDBanco`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuenta`
--

LOCK TABLES `cuenta` WRITE;
/*!40000 ALTER TABLE `cuenta` DISABLE KEYS */;
/*!40000 ALTER TABLE `cuenta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `OIDEmpresa` varchar(255) NOT NULL,
  `cuitEmpresa` varchar(255) NOT NULL,
  `fechaHabilitacionEmpresa` date DEFAULT NULL,
  `fechaInhabilitacionEmpresa` date DEFAULT NULL,
  `nombreEmpresa` varchar(50) NOT NULL,
  `OIDTipoEmpresa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDEmpresa`),
  UNIQUE KEY `UK_eaihovwjj0b6ipcwuau45fm98` (`cuitEmpresa`),
  KEY `FK_j2x2dwvhwnlxrakeev5f41x69` (`OIDTipoEmpresa`),
  CONSTRAINT `FK_j2x2dwvhwnlxrakeev5f41x69` FOREIGN KEY (`OIDTipoEmpresa`) REFERENCES `tipoempresa` (`OIDTipoEmpresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES ('1','1','2017-10-02',NULL,'Claro','1'),('2','2','2017-10-02',NULL,'ECOGas','1'),('3','3','2017-10-02',NULL,'Movistar','1');
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresatipoimpuesto`
--

DROP TABLE IF EXISTS `empresatipoimpuesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresatipoimpuesto` (
  `OIDEmpresaTipoImpuesto` varchar(255) NOT NULL,
  `codigoempresatipoimpuesto` int(11) NOT NULL,
  `periodoLiquidacion` int(11) DEFAULT NULL,
  `fechaInhabilitacionEmpresaTipoImpuesto` date DEFAULT NULL,
  `fechaHabilitacionEmpresaTipoImpuesto` date NOT NULL,
  `OIDTipoImpuesto` varchar(255) DEFAULT NULL,
  `OIDEmpresa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDEmpresaTipoImpuesto`),
  UNIQUE KEY `UK_qr2t4on5qvvo0kqky7hj14ohd` (`codigoempresatipoimpuesto`),
  KEY `FK_3vsmpg3qu09pxcr59r7jwq7wt` (`OIDTipoImpuesto`),
  KEY `FK_9rfnwxqnfdt39xws6vuurhq37` (`OIDEmpresa`),
  CONSTRAINT `FK_3vsmpg3qu09pxcr59r7jwq7wt` FOREIGN KEY (`OIDTipoImpuesto`) REFERENCES `tipoimpuesto` (`OIDTipoImpuesto`),
  CONSTRAINT `FK_9rfnwxqnfdt39xws6vuurhq37` FOREIGN KEY (`OIDEmpresa`) REFERENCES `empresa` (`OIDEmpresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresatipoimpuesto`
--

LOCK TABLES `empresatipoimpuesto` WRITE;
/*!40000 ALTER TABLE `empresatipoimpuesto` DISABLE KEYS */;
INSERT INTO `empresatipoimpuesto` VALUES ('1',1,1,NULL,'2017-10-02','1','1'),('2',2,2,NULL,'2017-08-01','2','2'),('3',3,1,NULL,'2017-10-02','1','3');
/*!40000 ALTER TABLE `empresatipoimpuesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresatipoimpuestoatributo`
--

DROP TABLE IF EXISTS `empresatipoimpuestoatributo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresatipoimpuestoatributo` (
  `OIDEmpresaTipoImpuestoAtributo` varchar(255) NOT NULL,
  `orden` int(11) DEFAULT NULL,
  `formato` varchar(255) DEFAULT NULL,
  `valorPeriodicidad` varchar(255) DEFAULT NULL,
  `OIDTipoImpuestoAtributo` varchar(32) DEFAULT NULL,
  `OIDEmpresaTipoImpuesto` varchar(255) NOT NULL,
  PRIMARY KEY (`OIDEmpresaTipoImpuestoAtributo`),
  KEY `FK_2q0tsyx31dogcnrgrff9opqua` (`OIDTipoImpuestoAtributo`),
  KEY `FK_m3qv64ld6mksnetr1ob1dtm2b` (`OIDEmpresaTipoImpuesto`),
  CONSTRAINT `FK_2q0tsyx31dogcnrgrff9opqua` FOREIGN KEY (`OIDTipoImpuestoAtributo`) REFERENCES `tipoimpuestoatributo` (`OIDTipoImpuestoAtributo`),
  CONSTRAINT `FK_m3qv64ld6mksnetr1ob1dtm2b` FOREIGN KEY (`OIDEmpresaTipoImpuesto`) REFERENCES `empresatipoimpuesto` (`OIDEmpresaTipoImpuesto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresatipoimpuestoatributo`
--

LOCK TABLES `empresatipoimpuestoatributo` WRITE;
/*!40000 ALTER TABLE `empresatipoimpuestoatributo` DISABLE KEYS */;
INSERT INTO `empresatipoimpuestoatributo` VALUES ('1',0,NULL,NULL,'1','1'),('10',3,NULL,NULL,'4','3'),('2',1,NULL,NULL,'2','1'),('3',2,NULL,NULL,'3','1'),('4',3,NULL,NULL,'4','1'),('5',0,NULL,NULL,'5','2'),('6',1,NULL,NULL,'6','2'),('7',0,NULL,NULL,'1','3'),('8',1,NULL,NULL,'2','3'),('9',2,NULL,NULL,'3','3');
/*!40000 ALTER TABLE `empresatipoimpuestoatributo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadocomisionempresasistema`
--

DROP TABLE IF EXISTS `estadocomisionempresasistema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadocomisionempresasistema` (
  `OIDEstadoComisionEmpresaSistema` varchar(255) NOT NULL,
  `fechaestadocesdesde` date DEFAULT NULL,
  `fechaestadoceshasta` date DEFAULT NULL,
  `OIDComisionEmpresaSistemaEstado` varchar(255) DEFAULT NULL,
  `OIDComisionEmpresaSistema` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDEstadoComisionEmpresaSistema`),
  KEY `FK_tid1eo550bguaqu6ey2q7ws4y` (`OIDComisionEmpresaSistemaEstado`),
  KEY `FK_tco1v39gugx2uhm8lm0c6shhd` (`OIDComisionEmpresaSistema`),
  CONSTRAINT `FK_tco1v39gugx2uhm8lm0c6shhd` FOREIGN KEY (`OIDComisionEmpresaSistema`) REFERENCES `comisionempresasistema` (`OIDComisionEmpresaSistema`),
  CONSTRAINT `FK_tid1eo550bguaqu6ey2q7ws4y` FOREIGN KEY (`OIDComisionEmpresaSistemaEstado`) REFERENCES `comisionempresasistemaestado` (`OIDComisionEmpresaSistemaEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadocomisionempresasistema`
--

LOCK TABLES `estadocomisionempresasistema` WRITE;
/*!40000 ALTER TABLE `estadocomisionempresasistema` DISABLE KEYS */;
INSERT INTO `estadocomisionempresasistema` VALUES ('402881e95f0401c6015f0401c8a00001','2017-10-09','2017-10-09','1','402881e95f0401c6015f0401c89c0000'),('402881e95f0401fb015f04026ad60000','2017-10-09',NULL,'3','402881e95f0401c6015f0401c89c0000');
/*!40000 ALTER TABLE `estadocomisionempresasistema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operacion`
--

DROP TABLE IF EXISTS `operacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operacion` (
  `OIDOperacion` varchar(255) NOT NULL,
  `codpagoelect` varchar(255) NOT NULL,
  `fechavencimientocomprobanteimpago` date NOT NULL,
  `fechaoperacion` date NOT NULL,
  `importeOperacion` double NOT NULL,
  `numeroComprobante` int(11) NOT NULL,
  `ComisionCalculadaOperacion` bit(1) DEFAULT NULL,
  `numerooperacion` int(11) NOT NULL,
  `OIDTipoImpuesto` varchar(255) DEFAULT NULL,
  `OIDEmpresaTipoImpuesto` varchar(255) DEFAULT NULL,
  `OIDCuenta` varchar(32) DEFAULT NULL,
  `OIDCliente` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`OIDOperacion`),
  KEY `FK_snguttj6rd8i3e75n1dl3bvx8` (`OIDTipoImpuesto`),
  KEY `FK_53os52ht01w28nbseqfk4qtpe` (`OIDEmpresaTipoImpuesto`),
  KEY `FK_njd548o9jlyb9ebfk9nsug2o2` (`OIDCuenta`),
  KEY `FK_9qxaeeke83rxu6yh5t454508f` (`OIDCliente`),
  CONSTRAINT `FK_53os52ht01w28nbseqfk4qtpe` FOREIGN KEY (`OIDEmpresaTipoImpuesto`) REFERENCES `empresatipoimpuesto` (`OIDEmpresaTipoImpuesto`),
  CONSTRAINT `FK_9qxaeeke83rxu6yh5t454508f` FOREIGN KEY (`OIDCliente`) REFERENCES `cliente` (`OIDCliente`),
  CONSTRAINT `FK_njd548o9jlyb9ebfk9nsug2o2` FOREIGN KEY (`OIDCuenta`) REFERENCES `cuenta` (`OIDCuenta`),
  CONSTRAINT `FK_snguttj6rd8i3e75n1dl3bvx8` FOREIGN KEY (`OIDTipoImpuesto`) REFERENCES `tipoimpuesto` (`OIDTipoImpuesto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operacion`
--

LOCK TABLES `operacion` WRITE;
/*!40000 ALTER TABLE `operacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `operacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operacionatributo`
--

DROP TABLE IF EXISTS `operacionatributo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operacionatributo` (
  `OIDOperacionAtributo` varchar(32) NOT NULL,
  `valorOperacionAtibuto` varchar(255) NOT NULL,
  `OIDTipoImpuestoAtributo` varchar(32) DEFAULT NULL,
  `OIDOperacion` varchar(255) NOT NULL,
  PRIMARY KEY (`OIDOperacionAtributo`),
  KEY `FK_qkhrhh46a9h00sain16mthy3u` (`OIDTipoImpuestoAtributo`),
  KEY `FK_of60sp4x3udcwwilb6kgbfyic` (`OIDOperacion`),
  CONSTRAINT `FK_of60sp4x3udcwwilb6kgbfyic` FOREIGN KEY (`OIDOperacion`) REFERENCES `operacion` (`OIDOperacion`),
  CONSTRAINT `FK_qkhrhh46a9h00sain16mthy3u` FOREIGN KEY (`OIDTipoImpuestoAtributo`) REFERENCES `tipoimpuestoatributo` (`OIDTipoImpuestoAtributo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operacionatributo`
--

LOCK TABLES `operacionatributo` WRITE;
/*!40000 ALTER TABLE `operacionatributo` DISABLE KEYS */;
/*!40000 ALTER TABLE `operacionatributo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametroconexion`
--

DROP TABLE IF EXISTS `parametroconexion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametroconexion` (
  `OIDParametroConexion` varchar(255) NOT NULL,
  `codigoConexion` varchar(255) NOT NULL,
  `codigoParametroConexion` int(11) NOT NULL,
  `fechaInhabilitacionParametroConexion` date DEFAULT NULL,
  `fechaHabilitacionParametroConexion` date NOT NULL,
  PRIMARY KEY (`OIDParametroConexion`),
  UNIQUE KEY `UK_fscgv0hrq02v1vrgciva20sno` (`codigoConexion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametroconexion`
--

LOCK TABLES `parametroconexion` WRITE;
/*!40000 ALTER TABLE `parametroconexion` DISABLE KEYS */;
INSERT INTO `parametroconexion` VALUES ('1','1',1,NULL,'2017-10-02');
/*!40000 ALTER TABLE `parametroconexion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametroporeditable`
--

DROP TABLE IF EXISTS `parametroporeditable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametroporeditable` (
  `OIDParametroPorEditable` varchar(255) NOT NULL,
  `porcentajecon` double NOT NULL,
  `porcentajesin` double NOT NULL,
  PRIMARY KEY (`OIDParametroPorEditable`),
  UNIQUE KEY `UK_eyyr02vvfglsxbsnpv7tmate6` (`porcentajecon`),
  UNIQUE KEY `UK_d4e7etita7nbldiogedy7f4um` (`porcentajesin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametroporeditable`
--

LOCK TABLES `parametroporeditable` WRITE;
/*!40000 ALTER TABLE `parametroporeditable` DISABLE KEYS */;
/*!40000 ALTER TABLE `parametroporeditable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametroporperiodicidad`
--

DROP TABLE IF EXISTS `parametroporperiodicidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametroporperiodicidad` (
  `OIDParametroPorPeriodicidad` varchar(255) NOT NULL,
  `porcentajeanual` double NOT NULL,
  `porcentajemensual` double NOT NULL,
  `porcentajebimestral` double NOT NULL,
  `porcentajetrimestral` double NOT NULL,
  PRIMARY KEY (`OIDParametroPorPeriodicidad`),
  UNIQUE KEY `UK_h2kbxlkifrjhtws5hxllv9x1n` (`porcentajeanual`),
  UNIQUE KEY `UK_9civj7aexs15hyc7l0u5u40r3` (`porcentajemensual`),
  UNIQUE KEY `UK_l4txao9cmcnj04ovgi6fm929j` (`porcentajebimestral`),
  UNIQUE KEY `UK_tabuncjoawx772i2uy5wvd86o` (`porcentajetrimestral`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametroporperiodicidad`
--

LOCK TABLES `parametroporperiodicidad` WRITE;
/*!40000 ALTER TABLE `parametroporperiodicidad` DISABLE KEYS */;
INSERT INTO `parametroporperiodicidad` VALUES ('1',0.5,0.5,0.5,0.5);
/*!40000 ALTER TABLE `parametroporperiodicidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol` (
  `OIDRol` varchar(32) NOT NULL,
  `fechaHabilitacionRol` date NOT NULL,
  `fechaInhabilitacionRol` date DEFAULT NULL,
  `codigoRol` int(11) DEFAULT NULL,
  `nombreRol` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDRol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES ('1','2017-10-02',NULL,1,'ClienteBanco'),('2','2017-10-02',NULL,2,'ClienteEmpresa'),('3','2017-10-02',NULL,3,'Admin');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rolopcion`
--

DROP TABLE IF EXISTS `rolopcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rolopcion` (
  `OIDRolOpcion` varchar(32) NOT NULL,
  `fechaHabilitacionRolOpcion` date NOT NULL,
  `fechaInhabilitacionRolOpcion` date DEFAULT NULL,
  `codigoRolOpcion` int(11) DEFAULT NULL,
  `nombreRolOpcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDRolOpcion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rolopcion`
--

LOCK TABLES `rolopcion` WRITE;
/*!40000 ALTER TABLE `rolopcion` DISABLE KEYS */;
INSERT INTO `rolopcion` VALUES ('1','2017-10-02',NULL,1,'CUPAGARIMPUESTO'),('2','2017-10-02',NULL,2,'CUCONSULTAROPERACIONES'),('3','2017-10-02',NULL,3,'CUEXPORTAROPERACIONES'),('4','2017-10-02',NULL,4,'CUCONFIRMARANULARCOMISION'),('5','2017-10-02',NULL,5,'CUABMTIPOIMPUESTO');
/*!40000 ALTER TABLE `rolopcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rolrolopcion`
--

DROP TABLE IF EXISTS `rolrolopcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rolrolopcion` (
  `OIDRol` varchar(32) NOT NULL,
  `OIDRolOpcion` varchar(32) NOT NULL,
  KEY `FK_dhrswql6a0tbxcvgx7ffayu2d` (`OIDRolOpcion`),
  KEY `FK_fp5nbbrj0ov6gs9or3cv5h2u8` (`OIDRol`),
  CONSTRAINT `FK_dhrswql6a0tbxcvgx7ffayu2d` FOREIGN KEY (`OIDRolOpcion`) REFERENCES `rolopcion` (`OIDRolOpcion`),
  CONSTRAINT `FK_fp5nbbrj0ov6gs9or3cv5h2u8` FOREIGN KEY (`OIDRol`) REFERENCES `rol` (`OIDRol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rolrolopcion`
--

LOCK TABLES `rolrolopcion` WRITE;
/*!40000 ALTER TABLE `rolrolopcion` DISABLE KEYS */;
INSERT INTO `rolrolopcion` VALUES ('1','1'),('1','2'),('2','3'),('3','5');
/*!40000 ALTER TABLE `rolrolopcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sistemabanco`
--

DROP TABLE IF EXISTS `sistemabanco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sistemabanco` (
  `OIDSistemaBanco` varchar(255) NOT NULL,
  `codigoSistemaBanco` int(11) NOT NULL,
  `fechaHabilitacionSistemaBanco` date NOT NULL,
  `fechaInhabilitacionSistemaBanco` date DEFAULT NULL,
  `OIDBanco` varchar(32) DEFAULT NULL,
  `OIDParametroConexion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDSistemaBanco`),
  UNIQUE KEY `UK_a75sm38e6fbpld17reh0a0ib4` (`codigoSistemaBanco`),
  KEY `FK_8c6cfku4ufh9frbnbqliyk2n1` (`OIDBanco`),
  KEY `FK_la4sx5kqaiisq0nvh6bvv4uwj` (`OIDParametroConexion`),
  CONSTRAINT `FK_8c6cfku4ufh9frbnbqliyk2n1` FOREIGN KEY (`OIDBanco`) REFERENCES `banco` (`OIDBanco`),
  CONSTRAINT `FK_la4sx5kqaiisq0nvh6bvv4uwj` FOREIGN KEY (`OIDParametroConexion`) REFERENCES `parametroconexion` (`OIDParametroConexion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sistemabanco`
--

LOCK TABLES `sistemabanco` WRITE;
/*!40000 ALTER TABLE `sistemabanco` DISABLE KEYS */;
INSERT INTO `sistemabanco` VALUES ('1',1,'2017-10-02',NULL,'1','1');
/*!40000 ALTER TABLE `sistemabanco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipocuenta`
--

DROP TABLE IF EXISTS `tipocuenta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipocuenta` (
  `OIDTipoCuenta` varchar(32) NOT NULL,
  `fechaHabilitacionTipoCuenta` date NOT NULL,
  `fechaInhabilitacionTipoCuenta` date DEFAULT NULL,
  `codigoTipoCuenta` int(11) DEFAULT NULL,
  `nombreTipoCuenta` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDTipoCuenta`),
  UNIQUE KEY `UK_qun6o1avgpd3eik7vyl6ycnfe` (`codigoTipoCuenta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipocuenta`
--

LOCK TABLES `tipocuenta` WRITE;
/*!40000 ALTER TABLE `tipocuenta` DISABLE KEYS */;
INSERT INTO `tipocuenta` VALUES ('1','2017-10-02',NULL,1,'Caja de ahorro');
/*!40000 ALTER TABLE `tipocuenta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipodato`
--

DROP TABLE IF EXISTS `tipodato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodato` (
  `OIDTipoDato` varchar(32) NOT NULL,
  `codTipoDato` int(11) NOT NULL,
  `nombreTipoDato` varchar(255) NOT NULL,
  `fechaHabilitacionTipoDato` date NOT NULL,
  `fechaInhabilitacionTipoDato` date DEFAULT NULL,
  PRIMARY KEY (`OIDTipoDato`),
  UNIQUE KEY `UK_prnreg6bfnobkot7veunh7v6b` (`codTipoDato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipodato`
--

LOCK TABLES `tipodato` WRITE;
/*!40000 ALTER TABLE `tipodato` DISABLE KEYS */;
INSERT INTO `tipodato` VALUES ('1',1,'Date','2017-10-02',NULL),('2',2,'double','2017-10-02',NULL);
/*!40000 ALTER TABLE `tipodato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoempresa`
--

DROP TABLE IF EXISTS `tipoempresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoempresa` (
  `OIDTipoEmpresa` varchar(255) NOT NULL,
  `codigoTipoEmpresa` int(11) NOT NULL,
  `nombreTipoEmpresa` varchar(255) NOT NULL,
  `fechaHabTipoEmpresa` date NOT NULL,
  `fechaInhabTipoEmpresa` date DEFAULT NULL,
  PRIMARY KEY (`OIDTipoEmpresa`),
  UNIQUE KEY `UK_8jttnniahw40yobjfej8hth62` (`codigoTipoEmpresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoempresa`
--

LOCK TABLES `tipoempresa` WRITE;
/*!40000 ALTER TABLE `tipoempresa` DISABLE KEYS */;
INSERT INTO `tipoempresa` VALUES ('1',1,'Servicios','2017-10-02',NULL);
/*!40000 ALTER TABLE `tipoempresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoimpuesto`
--

DROP TABLE IF EXISTS `tipoimpuesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoimpuesto` (
  `OIDTipoImpuesto` varchar(255) NOT NULL,
  `codigoTipoImpuesto` int(11) NOT NULL,
  `fechaHabilitacionTipoImpuesto` date NOT NULL,
  `fechaInhabilitacionTipoImpuesto` date DEFAULT NULL,
  `modificableTipoImpuesto` bit(1) NOT NULL,
  `periodicidad` varchar(50) DEFAULT NULL,
  `nombreTipoImpuesto` varchar(50) NOT NULL,
  PRIMARY KEY (`OIDTipoImpuesto`),
  UNIQUE KEY `UK_ixsqsw5v7ck9hkj80j8a73r72` (`codigoTipoImpuesto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoimpuesto`
--

LOCK TABLES `tipoimpuesto` WRITE;
/*!40000 ALTER TABLE `tipoimpuesto` DISABLE KEYS */;
INSERT INTO `tipoimpuesto` VALUES ('1',1,'2017-10-02',NULL,'','Mensual','Telefonia'),('2',2,'2017-10-02',NULL,'\0','Bimestral','Gas');
/*!40000 ALTER TABLE `tipoimpuesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoimpuestoatributo`
--

DROP TABLE IF EXISTS `tipoimpuestoatributo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoimpuestoatributo` (
  `OIDTipoImpuestoAtributo` varchar(32) NOT NULL,
  `codigoTipoImpuestoAtributo` int(11) NOT NULL,
  `OIDAtributo` varchar(32) DEFAULT NULL,
  `OIDTipoImpuesto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDTipoImpuestoAtributo`),
  KEY `FK_r302upy3nphgw4moutf12igk7` (`OIDAtributo`),
  KEY `FK_jh2wx2g90ti4lxwqa7gndbwfr` (`OIDTipoImpuesto`),
  CONSTRAINT `FK_jh2wx2g90ti4lxwqa7gndbwfr` FOREIGN KEY (`OIDTipoImpuesto`) REFERENCES `tipoimpuesto` (`OIDTipoImpuesto`),
  CONSTRAINT `FK_r302upy3nphgw4moutf12igk7` FOREIGN KEY (`OIDAtributo`) REFERENCES `atributo` (`OIDAtributo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoimpuestoatributo`
--

LOCK TABLES `tipoimpuestoatributo` WRITE;
/*!40000 ALTER TABLE `tipoimpuestoatributo` DISABLE KEYS */;
INSERT INTO `tipoimpuestoatributo` VALUES ('1',1,'1','1'),('2',2,'2','1'),('3',3,'3','1'),('4',4,'4','1'),('5',5,'1','2'),('6',6,'2','2');
/*!40000 ALTER TABLE `tipoimpuestoatributo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `OIDUsuario` varchar(255) NOT NULL,
  `codigoUsuario` int(11) NOT NULL,
  `nombreUsuario` varchar(50) NOT NULL,
  `contraseñaUsuario` varchar(50) NOT NULL,
  `fechaHabilitacionUsuario` date NOT NULL,
  `fechaInhabilitacionUsuario` date DEFAULT NULL,
  `OIDRol` varchar(32) DEFAULT NULL,
  `OIDCliente` varchar(32) DEFAULT NULL,
  `OIDEmpresa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OIDUsuario`),
  UNIQUE KEY `UK_7sigp9q9rn14h113enwi0qhfg` (`codigoUsuario`),
  UNIQUE KEY `UK_7mqtl03sff379x3519wvppefi` (`nombreUsuario`),
  KEY `FK_5avoy72x3xn8a1bhmmbj74aq2` (`OIDRol`),
  KEY `FK_2x6dsb0hwjh8oksk5khfvdk6u` (`OIDCliente`),
  KEY `FK_ksggwhm49gawaj0tg8u8y68na` (`OIDEmpresa`),
  CONSTRAINT `FK_2x6dsb0hwjh8oksk5khfvdk6u` FOREIGN KEY (`OIDCliente`) REFERENCES `cliente` (`OIDCliente`),
  CONSTRAINT `FK_5avoy72x3xn8a1bhmmbj74aq2` FOREIGN KEY (`OIDRol`) REFERENCES `rol` (`OIDRol`),
  CONSTRAINT `FK_ksggwhm49gawaj0tg8u8y68na` FOREIGN KEY (`OIDEmpresa`) REFERENCES `empresa` (`OIDEmpresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES ('1',1,'Fernando','123456','2017-10-02',NULL,'1','1',NULL),('2',2,'ECOGas','123','2017-10-02',NULL,'2',NULL,'2'),('3',3,'Admin','admin','2017-10-02',NULL,'3',NULL,NULL);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-20 22:17:10
