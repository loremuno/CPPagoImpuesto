create database if not exists pagoimpuesto;
use pagoimpuesto;

SET foreign_key_checks = 0 ; 
SET SQL_SAFE_UPDATES = 0;

delete from `atributo` ;
delete from `banco` ;
delete from `cliente` ;
delete from `comision` ;
delete from `comisionempresasistema` ;
delete from `comisionempresasistemaestado` ;
delete from `cuenta` ;
delete from `empresa` ;
delete from `empresatipoimpuesto` ;
delete from `empresatipoimpuestoatributo` ;
delete from `estadocomisionempresasistema` ;
delete from `operacion` ;
delete from `operacionatributo`;
delete from `parametroconexion` ;
delete from `parametroporeditable` ;
delete from `parametroporperiodicidad` ;

delete from `rol` ;
INSERT INTO `rol` (`OIDRol`, `fechaHabilitacionRol`, `fechaInhabilitacionRol`, `codigoRol`, `nombreRol`) 
VALUES
('1', '2017-10-19', NULL, 1, 'Admin');

delete from `rolopcion`;
INSERT INTO `rolopcion` (`OIDRolOpcion`, `fechaHabilitacionRolOpcion`, `fechaInhabilitacionRolOpcion`, `codigoRolOpcion`, `nombreRolOpcion`) 
VALUES ('1', '2017-10-02', NULL, 1, 'CUAsignarRol'),
('2', '2017-10-02', NULL, 2, 'CUABMRol'),
('3', '2017-10-02', NULL, 3, 'CUABMRolOpcion');

delete from `rolrolopcion` ;
INSERT INTO `rolrolopcion` (`OIDRol`, `OIDRolOpcion`) 
VALUES('1', '1'),('1', '2'),('1', '3');

delete from `sistemabanco`;
delete from `tipocuenta`;

delete from `tipodato`;
delete from `tipoempresa`;
delete from `tipoimpuesto`;
delete from `tipoimpuestoatributo`;

delete from `usuario`;
INSERT INTO `usuario` (`OIDUsuario`, `codigoUsuario`, `nombreUsuario`, `contraseñaUsuario`, `fechaHabilitacionUsuario`, `fechaInhabilitacionUsuario`, `OIDRol`, `OIDCliente`, `OIDEmpresa`)
VALUES('1', 1, 'Admin', 'admin', '2017-10-02', '2017-10-03', '1', NULL, NULL);

SET foreign_key_checks = 1 ; 
SET SQL_SAFE_UPDATES = 1;