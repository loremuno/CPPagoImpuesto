-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-10-2017 a las 00:09:28
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pagoimpuesto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `atributo`
--

CREATE TABLE `atributo` (
  `OIDAtributo` varchar(32) NOT NULL,
  `codigoAtributo` int(11) NOT NULL,
  `fechaHabilitacionAtributo` date NOT NULL,
  `fechaInhabilitacionAtributo` date DEFAULT NULL,
  `longitudAtributo` int(11) DEFAULT NULL,
  `nombreAtributo` varchar(45) NOT NULL,
  `OIDTipoDato` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `atributo`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco`
--

CREATE TABLE `banco` (
  `OIDBanco` varchar(32) NOT NULL,
  `fechaHabilitacionBanco` date DEFAULT NULL,
  `fechaInhabilitacionBanco` date DEFAULT NULL,
  `cuitBanco` varchar(255) DEFAULT NULL,
  `nombreBanco` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `banco`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `OIDCliente` varchar(32) NOT NULL,
  `apellidoCliente` varchar(255) NOT NULL,
  `dniCliente` int(11) NOT NULL,
  `nombreCliente` varchar(255) NOT NULL,
  `numeroCliente` varchar(255) NOT NULL,
  `fechaHabilitacionCliente` date DEFAULT NULL,
  `fechaInhabilitacionCliente` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comision`
--

CREATE TABLE `comision` (
  `OIDComision` varchar(255) NOT NULL,
  `valorcomision` double NOT NULL,
  `porcentajeUtilizado` double NOT NULL,
  `OIDOperacion` varchar(255) DEFAULT NULL,
  `OIDComisionEmpresaSistema` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comision`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comisionempresasistema`
--

CREATE TABLE `comisionempresasistema` (
  `OIDComisionEmpresaSistema` varchar(255) NOT NULL,
  `codigoces` int(11) NOT NULL,
  `fechacomisionempresasistema` date NOT NULL,
  `OIDEmpresaTipoImpuesto` varchar(255) DEFAULT NULL,
  `periodicidadUtilizada` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comisionempresasistema`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comisionempresasistemaestado`
--

CREATE TABLE `comisionempresasistemaestado` (
  `OIDComisionEmpresaSistemaEstado` varchar(255) NOT NULL,
  `codigoces` int(11) NOT NULL,
  `nombreEstadoCES` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comisionempresasistemaestado`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE `cuenta` (
  `OIDCuenta` varchar(32) NOT NULL,
  `fechaHabilitacionCuenta` date DEFAULT NULL,
  `fechaInhabilitacionCuenta` date DEFAULT NULL,
  `numeroCuenta` int(11) DEFAULT NULL,
  `OIDTipoCuenta` varchar(32) DEFAULT NULL,
  `OIDCliente` varchar(32) DEFAULT NULL,
  `OIDBanco` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuenta`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `OIDEmpresa` varchar(255) NOT NULL,
  `cuitEmpresa` varchar(255) NOT NULL,
  `fechaHabilitacionEmpresa` date DEFAULT NULL,
  `fechaInhabilitacionEmpresa` date DEFAULT NULL,
  `nombreEmpresa` varchar(50) NOT NULL,
  `OIDTipoEmpresa` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresa`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresatipoimpuesto`
--

CREATE TABLE `empresatipoimpuesto` (
  `OIDEmpresaTipoImpuesto` varchar(255) NOT NULL,
  `codigoempresatipoimpuesto` int(11) NOT NULL,
  `periodoLiquidacion` int(11) DEFAULT NULL,
  `fechaInhabilitacionEmpresaTipoImpuesto` date DEFAULT NULL,
  `fechaHabilitacionEmpresaTipoImpuesto` date NOT NULL,
  `OIDTipoImpuesto` varchar(255) DEFAULT NULL,
  `OIDEmpresa` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresatipoimpuesto`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresatipoimpuestoatributo`
--

CREATE TABLE `empresatipoimpuestoatributo` (
  `OIDEmpresaTipoImpuestoAtributo` varchar(255) NOT NULL,
  `orden` int(11) DEFAULT NULL,
  `formato` varchar(255) DEFAULT NULL,
  `valorPeriodicidad` varchar(255) DEFAULT NULL,
  `OIDTipoImpuestoAtributo` varchar(32) DEFAULT NULL,
  `OIDEmpresaTipoImpuesto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresatipoimpuestoatributo`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadocomisionempresasistema`
--

CREATE TABLE `estadocomisionempresasistema` (
  `OIDEstadoComisionEmpresaSistema` varchar(255) NOT NULL,
  `fechaestadocesdesde` date DEFAULT NULL,
  `fechaestadoceshasta` date DEFAULT NULL,
  `OIDComisionEmpresaSistemaEstado` varchar(255) DEFAULT NULL,
  `OIDComisionEmpresaSistema` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estadocomisionempresasistema`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operacion`
--

CREATE TABLE `operacion` (
  `OIDOperacion` varchar(255) NOT NULL,
  `codpagoelect` varchar(255) NOT NULL,
  `fechavencimientocomprobanteimpago` date NOT NULL,
  `fechaoperacion` date NOT NULL,
  `importeOperacion` double NOT NULL,
  `numeroComprobante` int(11) NOT NULL,
  `ComisionCalculadaOperacion` bit(1) DEFAULT NULL,
  `numerooperacion` int(11) NOT NULL,
  `OIDTipoImpuesto` varchar(255) DEFAULT NULL,
  `OIDEmpresaTipoImpuesto` varchar(255) DEFAULT NULL,
  `OIDCuenta` varchar(32) DEFAULT NULL,
  `OIDCliente` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `operacion`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operacionatributo`
--

CREATE TABLE `operacionatributo` (
  `OIDOperacionAtributo` varchar(32) NOT NULL,
  `valorOperacionAtibuto` varchar(255) NOT NULL,
  `OIDTipoImpuestoAtributo` varchar(32) DEFAULT NULL,
  `OIDOperacion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametroconexion`
--

CREATE TABLE `parametroconexion` (
  `OIDParametroConexion` varchar(255) NOT NULL,
  `codigoConexion` varchar(255) NOT NULL,
  `codigoParametroConexion` int(11) NOT NULL,
  `fechaInhabilitacionParametroConexion` date DEFAULT NULL,
  `fechaHabilitacionParametroConexion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `parametroconexion`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametroporeditable`
--

CREATE TABLE `parametroporeditable` (
  `OIDParametroPorEditable` varchar(255) NOT NULL,
  `porcentajecon` double NOT NULL,
  `porcentajesin` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametroporperiodicidad`
--

CREATE TABLE `parametroporperiodicidad` (
  `OIDParametroPorPeriodicidad` varchar(255) NOT NULL,
  `porcentajeanual` double NOT NULL,
  `porcentajemensual` double NOT NULL,
  `porcentajebimestral` double NOT NULL,
  `porcentajetrimestral` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `parametroporperiodicidad`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `OIDRol` varchar(32) NOT NULL,
  `fechaHabilitacionRol` date NOT NULL,
  `fechaInhabilitacionRol` date DEFAULT NULL,
  `codigoRol` int(11) DEFAULT NULL,
  `nombreRol` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--
INSERT INTO `rol` (`OIDRol`, `fechaHabilitacionRol`, `fechaInhabilitacionRol`, `codigoRol`, `nombreRol`) VALUES
('1', '2017-10-02', NULL, 1, 'ClienteBanco');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rolopcion`
--

CREATE TABLE `rolopcion` (
  `OIDRolOpcion` varchar(32) NOT NULL,
  `fechaHabilitacionRolOpcion` date NOT NULL,
  `fechaInhabilitacionRolOpcion` date DEFAULT NULL,
  `codigoRolOpcion` int(11) DEFAULT NULL,
  `nombreRolOpcion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rolopcion`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rolrolopcion`
--

CREATE TABLE `rolrolopcion` (
  `OIDRol` varchar(32) NOT NULL,
  `OIDRolOpcion` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rolrolopcion`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sistemabanco`
--

CREATE TABLE `sistemabanco` (
  `OIDSistemaBanco` varchar(255) NOT NULL,
  `codigoSistemaBanco` int(11) NOT NULL,
  `fechaHabilitacionSistemaBanco` date NOT NULL,
  `fechaInhabilitacionSistemaBanco` date DEFAULT NULL,
  `OIDBanco` varchar(32) DEFAULT NULL,
  `OIDParametroConexion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sistemabanco`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipocuenta`
--

CREATE TABLE `tipocuenta` (
  `OIDTipoCuenta` varchar(32) NOT NULL,
  `fechaHabilitacionTipoCuenta` date NOT NULL,
  `fechaInhabilitacionTipoCuenta` date DEFAULT NULL,
  `codigoTipoCuenta` int(11) DEFAULT NULL,
  `nombreTipoCuenta` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipocuenta`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodato`
--

CREATE TABLE `tipodato` (
  `OIDTipoDato` varchar(32) NOT NULL,
  `codTipoDato` int(11) NOT NULL,
  `nombreTipoDato` varchar(255) NOT NULL,
  `fechaHabilitacionTipoDato` date NOT NULL,
  `fechaInhabilitacionTipoDato` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipodato`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoempresa`
--

CREATE TABLE `tipoempresa` (
  `OIDTipoEmpresa` varchar(255) NOT NULL,
  `codigoTipoEmpresa` int(11) NOT NULL,
  `nombreTipoEmpresa` varchar(255) NOT NULL,
  `fechaHabTipoEmpresa` date NOT NULL,
  `fechaInhabTipoEmpresa` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoempresa`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoimpuesto`
--

CREATE TABLE `tipoimpuesto` (
  `OIDTipoImpuesto` varchar(255) NOT NULL,
  `codigoTipoImpuesto` int(11) NOT NULL,
  `fechaHabilitacionTipoImpuesto` date NOT NULL,
  `fechaInhabilitacionTipoImpuesto` date DEFAULT NULL,
  `modificableTipoImpuesto` bit(1) NOT NULL,
  `periodicidad` varchar(50) DEFAULT NULL,
  `nombreTipoImpuesto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoimpuesto`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoimpuestoatributo`
--

CREATE TABLE `tipoimpuestoatributo` (
  `OIDTipoImpuestoAtributo` varchar(32) NOT NULL,
  `codigoTipoImpuestoAtributo` int(11) NOT NULL,
  `OIDAtributo` varchar(32) DEFAULT NULL,
  `OIDTipoImpuesto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoimpuestoatributo`
--



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `OIDUsuario` varchar(255) NOT NULL,
  `codigoUsuario` int(11) NOT NULL,
  `nombreUsuario` varchar(50) NOT NULL,
  `contraseñaUsuario` varchar(50) NOT NULL,
  `fechaHabilitacionUsuario` date NOT NULL,
  `fechaInhabilitacionUsuario` date DEFAULT NULL,
  `OIDRol` varchar(32) DEFAULT NULL,
  `OIDCliente` varchar(32) DEFAULT NULL,
  `OIDEmpresa` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--
INSERT INTO `usuario` (`OIDUsuario`, `codigoUsuario`, `nombreUsuario`, `contraseñaUsuario`, `fechaHabilitacionUsuario`, `fechaInhabilitacionUsuario`, `OIDRol`, `OIDCliente`, `OIDEmpresa`) VALUES
('1', 1, 'carlos', '123456', '2017-10-02', NULL, '1', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `atributo`
--
ALTER TABLE `atributo`
  ADD PRIMARY KEY (`OIDAtributo`),
  ADD UNIQUE KEY `UK_d0yib8j696bhxicp6187olcbn` (`codigoAtributo`),
  ADD KEY `FK_bbgko1mw0ucuoga8wiovkxj75` (`OIDTipoDato`);

--
-- Indices de la tabla `banco`
--
ALTER TABLE `banco`
  ADD PRIMARY KEY (`OIDBanco`),
  ADD UNIQUE KEY `UK_1c70fmn989x0w3x3oe6kh3xex` (`cuitBanco`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`OIDCliente`),
  ADD UNIQUE KEY `UK_spjnf3pklvp1902gxxbhksaob` (`numeroCliente`);

--
-- Indices de la tabla `comision`
--
ALTER TABLE `comision`
  ADD PRIMARY KEY (`OIDComision`),
  ADD KEY `FK_7o63e1rj32iyd5ohqmj8lfx61` (`OIDOperacion`),
  ADD KEY `FK_myhupess14vq3vgqhw7xcu7an` (`OIDComisionEmpresaSistema`);

--
-- Indices de la tabla `comisionempresasistema`
--
ALTER TABLE `comisionempresasistema`
  ADD PRIMARY KEY (`OIDComisionEmpresaSistema`),
  ADD KEY `FK_8kqpv80oul55hms4gjvsi302` (`OIDEmpresaTipoImpuesto`);

--
-- Indices de la tabla `comisionempresasistemaestado`
--
ALTER TABLE `comisionempresasistemaestado`
  ADD PRIMARY KEY (`OIDComisionEmpresaSistemaEstado`),
  ADD UNIQUE KEY `UK_lijot92p9rcrgrdlgibbf58u7` (`codigoces`),
  ADD UNIQUE KEY `UK_3a63e5ekeqx7l0p0khd3w1cw4` (`nombreEstadoCES`);

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`OIDCuenta`),
  ADD KEY `FK_nsw6u6h0f2y7yrcxq99venu5y` (`OIDTipoCuenta`),
  ADD KEY `FK_3qxiemyc9uaws4aw8mbe9iy9c` (`OIDCliente`),
  ADD KEY `FK_psr41x6qd7t1m3i7jy4p6qx2s` (`OIDBanco`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`OIDEmpresa`),
  ADD UNIQUE KEY `UK_eaihovwjj0b6ipcwuau45fm98` (`cuitEmpresa`),
  ADD KEY `FK_j2x2dwvhwnlxrakeev5f41x69` (`OIDTipoEmpresa`);

--
-- Indices de la tabla `empresatipoimpuesto`
--
ALTER TABLE `empresatipoimpuesto`
  ADD PRIMARY KEY (`OIDEmpresaTipoImpuesto`),
  ADD UNIQUE KEY `UK_qr2t4on5qvvo0kqky7hj14ohd` (`codigoempresatipoimpuesto`),
  ADD KEY `FK_3vsmpg3qu09pxcr59r7jwq7wt` (`OIDTipoImpuesto`),
  ADD KEY `FK_9rfnwxqnfdt39xws6vuurhq37` (`OIDEmpresa`);

--
-- Indices de la tabla `empresatipoimpuestoatributo`
--
ALTER TABLE `empresatipoimpuestoatributo`
  ADD PRIMARY KEY (`OIDEmpresaTipoImpuestoAtributo`),
  ADD KEY `FK_2q0tsyx31dogcnrgrff9opqua` (`OIDTipoImpuestoAtributo`),
  ADD KEY `FK_m3qv64ld6mksnetr1ob1dtm2b` (`OIDEmpresaTipoImpuesto`);

--
-- Indices de la tabla `estadocomisionempresasistema`
--
ALTER TABLE `estadocomisionempresasistema`
  ADD PRIMARY KEY (`OIDEstadoComisionEmpresaSistema`),
  ADD KEY `FK_tid1eo550bguaqu6ey2q7ws4y` (`OIDComisionEmpresaSistemaEstado`),
  ADD KEY `FK_tco1v39gugx2uhm8lm0c6shhd` (`OIDComisionEmpresaSistema`);

--
-- Indices de la tabla `operacion`
--
ALTER TABLE `operacion`
  ADD PRIMARY KEY (`OIDOperacion`),
  ADD KEY `FK_snguttj6rd8i3e75n1dl3bvx8` (`OIDTipoImpuesto`),
  ADD KEY `FK_53os52ht01w28nbseqfk4qtpe` (`OIDEmpresaTipoImpuesto`),
  ADD KEY `FK_njd548o9jlyb9ebfk9nsug2o2` (`OIDCuenta`),
  ADD KEY `FK_9qxaeeke83rxu6yh5t454508f` (`OIDCliente`);

--
-- Indices de la tabla `operacionatributo`
--
ALTER TABLE `operacionatributo`
  ADD PRIMARY KEY (`OIDOperacionAtributo`),
  ADD KEY `FK_qkhrhh46a9h00sain16mthy3u` (`OIDTipoImpuestoAtributo`),
  ADD KEY `FK_of60sp4x3udcwwilb6kgbfyic` (`OIDOperacion`);

--
-- Indices de la tabla `parametroconexion`
--
ALTER TABLE `parametroconexion`
  ADD PRIMARY KEY (`OIDParametroConexion`),
  ADD UNIQUE KEY `UK_fscgv0hrq02v1vrgciva20sno` (`codigoConexion`);

--
-- Indices de la tabla `parametroporeditable`
--
ALTER TABLE `parametroporeditable`
  ADD PRIMARY KEY (`OIDParametroPorEditable`),
  ADD UNIQUE KEY `UK_eyyr02vvfglsxbsnpv7tmate6` (`porcentajecon`),
  ADD UNIQUE KEY `UK_d4e7etita7nbldiogedy7f4um` (`porcentajesin`);

--
-- Indices de la tabla `parametroporperiodicidad`
--
ALTER TABLE `parametroporperiodicidad`
  ADD PRIMARY KEY (`OIDParametroPorPeriodicidad`),
  ADD UNIQUE KEY `UK_h2kbxlkifrjhtws5hxllv9x1n` (`porcentajeanual`),
  ADD UNIQUE KEY `UK_9civj7aexs15hyc7l0u5u40r3` (`porcentajemensual`),
  ADD UNIQUE KEY `UK_l4txao9cmcnj04ovgi6fm929j` (`porcentajebimestral`),
  ADD UNIQUE KEY `UK_tabuncjoawx772i2uy5wvd86o` (`porcentajetrimestral`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`OIDRol`);

--
-- Indices de la tabla `rolopcion`
--
ALTER TABLE `rolopcion`
  ADD PRIMARY KEY (`OIDRolOpcion`);

--
-- Indices de la tabla `rolrolopcion`
--
ALTER TABLE `rolrolopcion`
  ADD KEY `FK_dhrswql6a0tbxcvgx7ffayu2d` (`OIDRolOpcion`),
  ADD KEY `FK_fp5nbbrj0ov6gs9or3cv5h2u8` (`OIDRol`);

--
-- Indices de la tabla `sistemabanco`
--
ALTER TABLE `sistemabanco`
  ADD PRIMARY KEY (`OIDSistemaBanco`),
  ADD UNIQUE KEY `UK_a75sm38e6fbpld17reh0a0ib4` (`codigoSistemaBanco`),
  ADD KEY `FK_8c6cfku4ufh9frbnbqliyk2n1` (`OIDBanco`),
  ADD KEY `FK_la4sx5kqaiisq0nvh6bvv4uwj` (`OIDParametroConexion`);

--
-- Indices de la tabla `tipocuenta`
--
ALTER TABLE `tipocuenta`
  ADD PRIMARY KEY (`OIDTipoCuenta`),
  ADD UNIQUE KEY `UK_qun6o1avgpd3eik7vyl6ycnfe` (`codigoTipoCuenta`);

--
-- Indices de la tabla `tipodato`
--
ALTER TABLE `tipodato`
  ADD PRIMARY KEY (`OIDTipoDato`),
  ADD UNIQUE KEY `UK_prnreg6bfnobkot7veunh7v6b` (`codTipoDato`);

--
-- Indices de la tabla `tipoempresa`
--
ALTER TABLE `tipoempresa`
  ADD PRIMARY KEY (`OIDTipoEmpresa`),
  ADD UNIQUE KEY `UK_8jttnniahw40yobjfej8hth62` (`codigoTipoEmpresa`);

--
-- Indices de la tabla `tipoimpuesto`
--
ALTER TABLE `tipoimpuesto`
  ADD PRIMARY KEY (`OIDTipoImpuesto`),
  ADD UNIQUE KEY `UK_ixsqsw5v7ck9hkj80j8a73r72` (`codigoTipoImpuesto`);

--
-- Indices de la tabla `tipoimpuestoatributo`
--
ALTER TABLE `tipoimpuestoatributo`
  ADD PRIMARY KEY (`OIDTipoImpuestoAtributo`),
  ADD KEY `FK_r302upy3nphgw4moutf12igk7` (`OIDAtributo`),
  ADD KEY `FK_jh2wx2g90ti4lxwqa7gndbwfr` (`OIDTipoImpuesto`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`OIDUsuario`),
  ADD UNIQUE KEY `UK_7sigp9q9rn14h113enwi0qhfg` (`codigoUsuario`),
  ADD UNIQUE KEY `UK_7mqtl03sff379x3519wvppefi` (`nombreUsuario`),
  ADD KEY `FK_5avoy72x3xn8a1bhmmbj74aq2` (`OIDRol`),
  ADD KEY `FK_2x6dsb0hwjh8oksk5khfvdk6u` (`OIDCliente`),
  ADD KEY `FK_ksggwhm49gawaj0tg8u8y68na` (`OIDEmpresa`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `atributo`
--
ALTER TABLE `atributo`
  ADD CONSTRAINT `FK_bbgko1mw0ucuoga8wiovkxj75` FOREIGN KEY (`OIDTipoDato`) REFERENCES `tipodato` (`OIDTipoDato`);

--
-- Filtros para la tabla `comision`
--
ALTER TABLE `comision`
  ADD CONSTRAINT `FK_7o63e1rj32iyd5ohqmj8lfx61` FOREIGN KEY (`OIDOperacion`) REFERENCES `operacion` (`OIDOperacion`),
  ADD CONSTRAINT `FK_myhupess14vq3vgqhw7xcu7an` FOREIGN KEY (`OIDComisionEmpresaSistema`) REFERENCES `comisionempresasistema` (`OIDComisionEmpresaSistema`);

--
-- Filtros para la tabla `comisionempresasistema`
--
ALTER TABLE `comisionempresasistema`
  ADD CONSTRAINT `FK_8kqpv80oul55hms4gjvsi302` FOREIGN KEY (`OIDEmpresaTipoImpuesto`) REFERENCES `empresatipoimpuesto` (`OIDEmpresaTipoImpuesto`);

--
-- Filtros para la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD CONSTRAINT `FK_3qxiemyc9uaws4aw8mbe9iy9c` FOREIGN KEY (`OIDCliente`) REFERENCES `cliente` (`OIDCliente`),
  ADD CONSTRAINT `FK_nsw6u6h0f2y7yrcxq99venu5y` FOREIGN KEY (`OIDTipoCuenta`) REFERENCES `tipocuenta` (`OIDTipoCuenta`),
  ADD CONSTRAINT `FK_psr41x6qd7t1m3i7jy4p6qx2s` FOREIGN KEY (`OIDBanco`) REFERENCES `banco` (`OIDBanco`);

--
-- Filtros para la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD CONSTRAINT `FK_j2x2dwvhwnlxrakeev5f41x69` FOREIGN KEY (`OIDTipoEmpresa`) REFERENCES `tipoempresa` (`OIDTipoEmpresa`);

--
-- Filtros para la tabla `empresatipoimpuesto`
--
ALTER TABLE `empresatipoimpuesto`
  ADD CONSTRAINT `FK_3vsmpg3qu09pxcr59r7jwq7wt` FOREIGN KEY (`OIDTipoImpuesto`) REFERENCES `tipoimpuesto` (`OIDTipoImpuesto`),
  ADD CONSTRAINT `FK_9rfnwxqnfdt39xws6vuurhq37` FOREIGN KEY (`OIDEmpresa`) REFERENCES `empresa` (`OIDEmpresa`);

--
-- Filtros para la tabla `empresatipoimpuestoatributo`
--
ALTER TABLE `empresatipoimpuestoatributo`
  ADD CONSTRAINT `FK_2q0tsyx31dogcnrgrff9opqua` FOREIGN KEY (`OIDTipoImpuestoAtributo`) REFERENCES `tipoimpuestoatributo` (`OIDTipoImpuestoAtributo`),
  ADD CONSTRAINT `FK_m3qv64ld6mksnetr1ob1dtm2b` FOREIGN KEY (`OIDEmpresaTipoImpuesto`) REFERENCES `empresatipoimpuesto` (`OIDEmpresaTipoImpuesto`);

--
-- Filtros para la tabla `estadocomisionempresasistema`
--
ALTER TABLE `estadocomisionempresasistema`
  ADD CONSTRAINT `FK_tco1v39gugx2uhm8lm0c6shhd` FOREIGN KEY (`OIDComisionEmpresaSistema`) REFERENCES `comisionempresasistema` (`OIDComisionEmpresaSistema`),
  ADD CONSTRAINT `FK_tid1eo550bguaqu6ey2q7ws4y` FOREIGN KEY (`OIDComisionEmpresaSistemaEstado`) REFERENCES `comisionempresasistemaestado` (`OIDComisionEmpresaSistemaEstado`);

--
-- Filtros para la tabla `operacion`
--
ALTER TABLE `operacion`
  ADD CONSTRAINT `FK_53os52ht01w28nbseqfk4qtpe` FOREIGN KEY (`OIDEmpresaTipoImpuesto`) REFERENCES `empresatipoimpuesto` (`OIDEmpresaTipoImpuesto`),
  ADD CONSTRAINT `FK_9qxaeeke83rxu6yh5t454508f` FOREIGN KEY (`OIDCliente`) REFERENCES `cliente` (`OIDCliente`),
  ADD CONSTRAINT `FK_njd548o9jlyb9ebfk9nsug2o2` FOREIGN KEY (`OIDCuenta`) REFERENCES `cuenta` (`OIDCuenta`),
  ADD CONSTRAINT `FK_snguttj6rd8i3e75n1dl3bvx8` FOREIGN KEY (`OIDTipoImpuesto`) REFERENCES `tipoimpuesto` (`OIDTipoImpuesto`);

--
-- Filtros para la tabla `operacionatributo`
--
ALTER TABLE `operacionatributo`
  ADD CONSTRAINT `FK_of60sp4x3udcwwilb6kgbfyic` FOREIGN KEY (`OIDOperacion`) REFERENCES `operacion` (`OIDOperacion`),
  ADD CONSTRAINT `FK_qkhrhh46a9h00sain16mthy3u` FOREIGN KEY (`OIDTipoImpuestoAtributo`) REFERENCES `tipoimpuestoatributo` (`OIDTipoImpuestoAtributo`);

--
-- Filtros para la tabla `rolrolopcion`
--
ALTER TABLE `rolrolopcion`
  ADD CONSTRAINT `FK_dhrswql6a0tbxcvgx7ffayu2d` FOREIGN KEY (`OIDRolOpcion`) REFERENCES `rolopcion` (`OIDRolOpcion`),
  ADD CONSTRAINT `FK_fp5nbbrj0ov6gs9or3cv5h2u8` FOREIGN KEY (`OIDRol`) REFERENCES `rol` (`OIDRol`);

--
-- Filtros para la tabla `sistemabanco`
--
ALTER TABLE `sistemabanco`
  ADD CONSTRAINT `FK_8c6cfku4ufh9frbnbqliyk2n1` FOREIGN KEY (`OIDBanco`) REFERENCES `banco` (`OIDBanco`),
  ADD CONSTRAINT `FK_la4sx5kqaiisq0nvh6bvv4uwj` FOREIGN KEY (`OIDParametroConexion`) REFERENCES `parametroconexion` (`OIDParametroConexion`);

--
-- Filtros para la tabla `tipoimpuestoatributo`
--
ALTER TABLE `tipoimpuestoatributo`
  ADD CONSTRAINT `FK_jh2wx2g90ti4lxwqa7gndbwfr` FOREIGN KEY (`OIDTipoImpuesto`) REFERENCES `tipoimpuesto` (`OIDTipoImpuesto`),
  ADD CONSTRAINT `FK_r302upy3nphgw4moutf12igk7` FOREIGN KEY (`OIDAtributo`) REFERENCES `atributo` (`OIDAtributo`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `FK_2x6dsb0hwjh8oksk5khfvdk6u` FOREIGN KEY (`OIDCliente`) REFERENCES `cliente` (`OIDCliente`),
  ADD CONSTRAINT `FK_5avoy72x3xn8a1bhmmbj74aq2` FOREIGN KEY (`OIDRol`) REFERENCES `rol` (`OIDRol`),
  ADD CONSTRAINT `FK_ksggwhm49gawaj0tg8u8y68na` FOREIGN KEY (`OIDEmpresa`) REFERENCES `empresa` (`OIDEmpresa`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
