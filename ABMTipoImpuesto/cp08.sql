create database if not exists pagoimpuesto;
use pagoimpuesto;

SET foreign_key_checks = 0 ; 
SET SQL_SAFE_UPDATES = 0;

delete from `atributo` ;
INSERT INTO `atributo` (`OIDAtributo`, `codigoAtributo`, `fechaHabilitacionAtributo`, `fechaInhabilitacionAtributo`, `longitudAtributo`, `nombreAtributo`, `OIDTipoDato`) VALUES
('1', 1, '2017-10-02', null, NULL, 'Importe', '2'),
('2', 2, '2017-10-02', null, NULL, 'Fecha de vencimiento', '1');

delete from `banco` ;
delete from `cliente` ;
delete from `comision` ;
delete from `comisionempresasistema` ;
delete from `comisionempresasistemaestado` ;
delete from `cuenta` ;
delete from `empresa` ;
delete from `empresatipoimpuesto` ;
delete from `empresatipoimpuestoatributo` ;
delete from `estadocomisionempresasistema` ;
delete from `operacion` ;
delete from `operacionatributo`;
delete from `parametroconexion` ;
delete from `parametroporeditable` ;
delete from `parametroporperiodicidad` ;

delete from `rol` ;
INSERT INTO `rol` (`OIDRol`, `fechaHabilitacionRol`, `fechaInhabilitacionRol`, `codigoRol`, `nombreRol`) VALUES
('1', '2017-10-19', NULL, 1, 'Admin');

delete from `rolopcion`;
INSERT INTO `rolopcion` (`OIDRolOpcion`, `fechaHabilitacionRolOpcion`, `fechaInhabilitacionRolOpcion`, `codigoRolOpcion`, `nombreRolOpcion`) 
VALUES ('1', '2017-10-02', NULL, 1, 'CUAsignarRol'),
	   ('2', '2017-10-02', NULL, 2, 'CUABMTipoImpuesto');

delete from `rolrolopcion` ;
INSERT INTO `rolrolopcion` (`OIDRol`, `OIDRolOpcion`) 
VALUES('1', '1'),('1','2');

delete from `sistemabanco`;
delete from `tipocuenta`;

delete from `tipodato`;
INSERT INTO `tipodato` (`OIDTipoDato`, `codTipoDato`, `nombreTipoDato`, `fechaHabilitacionTipoDato`, `fechaInhabilitacionTipoDato`) VALUES
('1', 1, 'Date', '2017-10-02', NULL),
('2', 2, 'double', '2017-10-02', NULL);

delete from `tipoempresa`;

delete from `tipoimpuesto`;
INSERT INTO `tipoimpuesto` (`OIDTipoImpuesto`, `codigoTipoImpuesto`, `fechaHabilitacionTipoImpuesto`, `fechaInhabilitacionTipoImpuesto`, `modificableTipoImpuesto`, `periodicidad`, `nombreTipoImpuesto`) 
VALUES('1', 1, '2017-10-02', NULL, b'1', 'Mensual', 'Telefonia'),
('2', 2, '2017-10-02', NULL, b'1', 'Bimestral', 'Gas');

delete from `tipoimpuestoatributo`;
INSERT INTO `tipoimpuestoatributo` (`OIDTipoImpuestoAtributo`, `codigoTipoImpuestoAtributo`, `OIDAtributo`, `OIDTipoImpuesto`) VALUES
('1', 1, '1', '1'),
('2', 2, '2', '1'),
('3', 3, '1', '2'),
('4', 4, '2', '2');

delete from `usuario`;
INSERT INTO `usuario` (`OIDUsuario`, `codigoUsuario`, `nombreUsuario`, `contraseñaUsuario`, `fechaHabilitacionUsuario`, `fechaInhabilitacionUsuario`, `OIDRol`, `OIDCliente`, `OIDEmpresa`)
VALUES('1', 1, 'Admin', 'admin', '2017-10-02', NULL, '1', NULL, NULL);

SET foreign_key_checks = 1 ; 
SET SQL_SAFE_UPDATES = 1;